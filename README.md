![logo.png](https://bitbucket.org/repo/7orM5x/images/4028782738-logo.png)

# Match analyzer #

Aplikacja w języku Java wspomagająca analizowanie meczów piłkarskich z wykorzystaniem frameworka VLCj. W implementacji programu wykorzystano nowe możliwości języka Java w wersji 8. Interfejs graficzny oprogramowania oparty został na platformie JavaFX. Stworzoną aplikację napisano w zintegrowanym środowisku programistycznym
Intellij IDEA. Biblioteka iText została wykorzystana do eksportowania wyników aplikacji do pliku PDF. W aplikacji został wykorzystany Gradle do zarządzania bibliotekami zewnętrznymi oraz pakowania kodu binarnego dla najpopularniejszych systemów operacyjnych. Pozostałe wykorzystane biblioteki to ControlsFX, FontAwesomeFX, JFreeChart i Logback.

## VLCj ##

VLCj jest frameworkiem z otwartym źródłem, który pozwala na osadzenie instancji
odtwarzacza VLC (ang. VideoLAN Client) w aplikacji napisanej w języku Java. [Link do projektu](http://capricasoftware.co.uk/#/projects/vlcj)

## Dłuższy opis aplikacji ##

Oprogramowanie łączy w sobie funkcje odtwarzacza multimedialnego oraz modułu do gromadzenia i analizowania informacji zebranych podczas oglądania meczów piłkarskich. Zbierane są informacje na temat zdarzeń piłkarskich, jakie miały miejsce na boisku. Przykładami takich zdarzeń mogą być podania, strzały czy dośrodkowania. Następnie na podstawie zebranych informacji tworzone są pewnego rodzaju zestawienia oraz analizy. Istnieje możliwość wygenerowana wyników do pliku PDF. 

Informacje na temat drużyn i ich zawodników zapisywane są w bazie danych. Aplikacja posiada dwie dodatkowe bazy – krajów i pozycji piłkarskich. Poza informacjami na temat krajów przechowywane są grafiki flag narodowych. Obie dodatkowe bazy nie mogą być modyfikowane przez użytkowników programu. Baza danych drużyn przechowywana jest w pliku XML. Istnieje możliwość eksportu oraz importu bazy drużyn. Aplikacja pozwala na zapis oraz odczyt historii meczu do/z pliku XML.

## Wymagania funkcjonalne aplikacji ##

Do wymagań funkcjonalnych należą m.in. następujące elementy:

* odtwarzanie filmów w najpopularniejszych formatach multimedialnych,

* dodawanie, edytowanie oraz usuwanie informacji o drużynie w bazie danych
aplikacji,

* dodawanie, edytowanie oraz usuwanie informacji o zawodnikach drużyny
w bazie danych aplikacji,

* eksportowanie oraz importowanie bazy danych aplikacji do/z pliku XML,

* rejestrowanie przez użytkownika zdarzeń piłkarskich mających miejsce na
boisku w trakcie trwania meczu przy użyciu techniki przeciągnij i upuść,

* rejestrowanie zdarzeń, w których bierze udział jeden lub dwóch zawodników
tej samej drużyny,

* rozpoczęcie zapisywania zdarzenia powoduje zatrzymanie odtwarzanego
meczu, a zakończenie ponowne uruchomienie filmu,

* przeprowadzanie zmian zawodników na wirtualnym boisku,

* rozmieszczenie zawodników na wirtualnych boisku w dowolnej formacji
piłkarskiej,

* edytowanie składu drużyny, dla której rejestrujemy zdarzenia piłkarskie,

* dodawanie i usuwanie zdarzeń piłkarskich do historii analizowanego meczu,

* zapis oraz odczyt zebranych informacji do/z pliku XML,

* generowanie zestawień oraz analiz na podstawie zebranych zdarzeń piłkarskich,

* wyeksportowanie wyników do pliku PDF,

* rejestrowanie oraz zapisywanie operacji wykonywanych w aplikacji do plików
z logami.

## Interfejs graficzny ##


![app1.png](https://bitbucket.org/repo/7orM5x/images/2699719798-app1.png)

Widok odtwarzacza filmów ze stworzoną historią meczu

![app4 wybrana druzyna.png](https://bitbucket.org/repo/7orM5x/images/129640383-app4%20wybrana%20druzyna.png)

Widok bazy drużyn z wybraną drużyną