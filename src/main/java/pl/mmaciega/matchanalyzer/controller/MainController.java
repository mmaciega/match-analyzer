package pl.mmaciega.matchanalyzer.controller;

import com.itextpdf.text.DocumentException;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.stage.*;
import org.controlsfx.dialog.Wizard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.mmaciega.matchanalyzer.DataManager;
import pl.mmaciega.matchanalyzer.Main;
import pl.mmaciega.matchanalyzer.model.MatchHistory;
import pl.mmaciega.matchanalyzer.model.Team;
import pl.mmaciega.matchanalyzer.model.events.FootballEvent;
import pl.mmaciega.matchanalyzer.pdf.DocumentPdf;
import pl.mmaciega.matchanalyzer.ui.CreateMatchHistoryWizard;
import pl.mmaciega.matchanalyzer.ui.ResizableMediaPlayer;
import pl.mmaciega.matchanalyzer.util.jaxb.wrapers.MatchHistoryWrapper;

import javax.xml.bind.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;

public class MainController {
    @FXML
    private Menu recentlyOpenedFilesMenu;
    @FXML
    private BorderPane database;
    @FXML
    private DatabaseController databaseController;
    @FXML
    private BorderPane mediaPlayer;
    @FXML
    private MediaPlayerController mediaPlayerController;

    @FXML
    private RadioMenuItem firstHalfRadio;
    @FXML
    private RadioMenuItem secondHalfRadio;
    @FXML
    private ToggleGroup halfsMenu;
    @FXML
    private Menu halfMenu;
    @FXML
    private MenuItem saveMenuItem;
    @FXML
    private MenuItem saveAsMenuItem;
    @FXML
    private MenuItem exportPdfMenuItem;

    private Stage stage;
    private int numStartElemRecentlyFilesMenu;

    private ObservableList<Team> teams = FXCollections.observableArrayList();
    private ObjectProperty<MatchHistory> currentMatchHistory = new SimpleObjectProperty<>();

    private final Logger logger = LoggerFactory.getLogger(MainController.class);

    private final Preferences preferences = Preferences.userNodeForPackage(MainController.class);

    private static final int MAX_LETTERS_FOR_FILEPATH = 70;
    private static final String RECENTLY_OPENED_FILES_PREF_KEY = "recentlyOpenedFiles";


    @FXML
    private void initialize() {
        logger.info("Initialize");

        halfMenu.disableProperty().bind(currentMatchHistory.isNull());
        saveMenuItem.disableProperty().bind(currentMatchHistory.isNull());
        saveAsMenuItem.disableProperty().bind(currentMatchHistory.isNull());
        exportPdfMenuItem.disableProperty().bind(currentMatchHistory.isNull());

        firstHalfRadio.setUserData(FootballEvent.Halfs.FIRST);
        secondHalfRadio.setUserData(FootballEvent.Halfs.SECOND);

        numStartElemRecentlyFilesMenu = recentlyOpenedFilesMenu.getItems().size();

        updateRecentlyOpenedFilesMenu();

        databaseController.init(this);
        mediaPlayerController.init(this);

        database.setVisible(false);
        database.visibleProperty().bind(mediaPlayer.visibleProperty().not());
        recentlyOpenedFilesMenu.disableProperty().bind(Bindings.lessThanOrEqual(Bindings.size(recentlyOpenedFilesMenu.getItems()), numStartElemRecentlyFilesMenu));

//        preferences.remove(RECENTLY_OPENED_FILES_PREF_KEY);
    }

    private void updateRecentlyOpenedFilesMenu() {
        updateRecentlyOpenedFilesMenu(getRecentlyOpenedFilesPreference());
    }

    private void updateRecentlyOpenedFilesMenu(String newPath) {
        List<String> filesPaths = addRecentlyOpenedFile(newPath);
        updateRecentlyOpenedFilesMenu(filesPaths);
    }

    private List<String> addRecentlyOpenedFile(String path) {
        List<String> filesPaths = getRecentlyOpenedFilesPreference();
        filesPaths.add(0, path);
        filesPaths = filesPaths.stream().limit(10).distinct().collect(Collectors.toList());

        setRecentlyOpenedFilesPreference(filesPaths);

        return filesPaths;
    }

    private List<String> getRecentlyOpenedFilesPreference() {
        String recentlyOpenedFiles = preferences.get(RECENTLY_OPENED_FILES_PREF_KEY, "");
        logger.info("Ostatnio przed dodaniem: " + recentlyOpenedFiles);

        if (recentlyOpenedFiles.isEmpty()) {
            return new ArrayList<>();
        } else {
            return new ArrayList<>(Arrays.asList(recentlyOpenedFiles.split("\\|")));
        }
    }

    private void updateRecentlyOpenedFilesMenu(List<String> filesPaths) {
        logger.info("Files paths: " + filesPaths);

        removeRecentlyOpenedFilesMenuItems();

        filesPaths.stream()
                .collect(Collectors.toCollection(LinkedList::new))
                .descendingIterator()
                .forEachRemaining(filePath -> {
                    int start = filePath.length() - MAX_LETTERS_FOR_FILEPATH;
                    String shortPath = (start > 0 ? "..." : "") + filePath.substring(start < 0 ? 0 : start);

                    MenuItem menuItem = new MenuItem(shortPath);
                    menuItem.setOnAction(event -> playMediaAndUpdateUI(new File(filePath)));
                    recentlyOpenedFilesMenu.getItems().add(0, menuItem);
                });
    }

    private void removeRecentlyOpenedFilesMenuItems() {
        ObservableList<MenuItem> items = recentlyOpenedFilesMenu.getItems();
        items.remove(0, items.size() - numStartElemRecentlyFilesMenu);
    }

    @FXML
    public void ejectFile() {
        FileChooser.ExtensionFilter videoFilter = new FileChooser.ExtensionFilter("Pliki video", "*.mp4", "*.avi", "*.mkv");
        FileChooser.ExtensionFilter allFilter = new FileChooser.ExtensionFilter("Wszystkie pliki (*)", ";*.*;");

        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(videoFilter, allFilter);

        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            playMediaAndUpdateUI(file);
        }
    }

    private void playMediaAndUpdateUI(File file) {
        String absolutePath = file.getAbsolutePath();

        Task<Boolean> task = new Task<Boolean>() {
            @Override
            protected Boolean call() throws Exception {
                boolean startMedia = ResizableMediaPlayer.getInstance().getMediaPlayer().startMedia(absolutePath);
                if (startMedia) {
                    ResizableMediaPlayer.getInstance().getMediaPlayer().setSpu(-1);
                }
                return startMedia;
            }
        };
        new Thread(task).start();

        updateRecentlyOpenedFilesMenu(absolutePath);
    }

    @FXML
    private void clearRecentlyOpenedFiles() {
        removeRecentlyOpenedFilesMenuItems();
        setRecentlyOpenedFilesPreference(null);
    }

    private void setRecentlyOpenedFilesPreference(List<String> filesPaths) {
        String recentlyOpenedFiles = "";
        if (filesPaths != null) {
            recentlyOpenedFiles = filesPaths.stream().reduce((t, u) -> t + "|" + u).get();
        }

        logger.info("Ostatnio po dodaniu: " + recentlyOpenedFiles);
        preferences.put(RECENTLY_OPENED_FILES_PREF_KEY, recentlyOpenedFiles);
    }

    @FXML
    private void changeToPlayerPanel() {
        mediaPlayer.toFront();
        mediaPlayer.setVisible(true);
    }

    @FXML
    private void changeToDatabasePanel() {
        database.toFront();
        mediaPlayer.setVisible(false);
    }

    @FXML
    private void saveDatabase() {
        DataManager.saveTeamDatabase(getTeams());
    }

    @FXML
    private void importDatabase() {
        FileChooser.ExtensionFilter xmlFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Import bazy danych drużyn");
        fileChooser.getExtensionFilters().add(xmlFilter);

        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            Optional<ButtonType> result = showConfirmation("Potwierdź import bazy danych drużyn"
                    , "Czy na pewno chcesz zaimportować baze danych drużyn?"
                    , "Zaimportowanie spowoduje zamianę istniejącej co skutuje utratą aktualnej bazy danych.");

            if (result.get() == ButtonType.OK) {
                try {
                    teams.setAll(DataManager.importTeamDatabase(file));
                } catch (JAXBException e) {
                    showError("Błąd importu bazy danych drużyn", "Błąd bazy zespołów", "Podczas ładowania pliku bazy danych drużyn wystąpił błąd. Import zakończony niepowodzeniem.");
                }
            }

        }
    }

    @FXML
    private void exportDatabase() {
        FileChooser.ExtensionFilter xmlFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Eksport bazy danych drużyn");
        fileChooser.getExtensionFilters().add(xmlFilter);

        File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            if (!file.getPath().endsWith(".xml")) {
                file = new File(file.getPath() + ".xml");
            }

            DataManager.exportTeamDatabase(file, getTeams());
        }
    }

    @FXML
    private void exportToPdf() {
        FileChooser.ExtensionFilter xmlFilter = new FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.pdf");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Eksport historii meczu do pliku PDF");
        fileChooser.getExtensionFilters().add(xmlFilter);

        File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            if (!file.getPath().endsWith(".pdf")) {
                file = new File(file.getPath() + ".pdf");
            }

            final File finalFile = file;
            new Thread(() -> {
                try {
                    DocumentPdf documentPdf = new DocumentPdf(currentMatchHistory.get(), finalFile.getPath());
                    documentPdf.generate();
                    Desktop.getDesktop().open(finalFile);
                } catch (DocumentException | IOException e) {
                    showError("Błąd eksportu historii meczu do pliku PDF", "Błąd eksportu do PDF", "Podczas tworzenia pliku PDF historii meczu wystąpił błąd.");
                    logger.error("Błąd podczas eksportowania historii meczu do pliku PDF", e);
                }
            }).start();
        }
    }

    @FXML
    private void closeApplication() {
        stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
    }

    @FXML
    private void showAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(Main.class.getResource("/view/About.fxml"));
        try {
            DialogPane dialogPane = fxmlLoader.load();
            alert.setDialogPane(dialogPane);
            alert.setTitle("O programie Match Analyzer");
            alert.getButtonTypes().add(ButtonType.OK);
            alert.initStyle(StageStyle.UTILITY);
            alert.initModality(Modality.WINDOW_MODAL);
            alert.initOwner(stage);
            alert.showAndWait();
        } catch (IOException e) {
            showError("Błąd podczas otwierania okna \"O programie\"", "", "Wystąpił nieoczekiwany błąd podczas otwierania okna");
            logger.error("Błąd podczas otwierania okna \"O programie\"", e);
        }
    }

    @FXML
    private void showNewHistory() {
        try {
            Wizard wizard = new CreateMatchHistoryWizard(getStage(), teams, mediaPlayerController.getPositionGraphicList());
            wizard.showAndWait().ifPresent(buttonType -> {
                if (buttonType.getButtonData() == ButtonBar.ButtonData.FINISH) {
                    MatchHistory matchHistory = (MatchHistory) wizard.getSettings().get("matchHistory");
                    currentMatchHistory.set(matchHistory);
                    getStage().setTitle(currentMatchHistory.get().getHostTeam().getName() + " "
                            + currentMatchHistory.get().getGuestTeam().getName() + " - " + Main.APP_TITLE);
                    DataManager.removeFilePath();
                    System.out.println(matchHistory);
                }
            });
        } catch (IOException e) {
            showError("Błąd podczas otwierania okna \"O programie\"", "", "Wystąpił nieoczekiwany błąd podczas otwierania okna");
            logger.error("Błąd podczas otwierania okna \"Nowa historia meczu\"", e);
        }
    }

    @FXML
    private void saveMatchHistory() {
        try {
            MatchHistoryWrapper matchHistoryWrapper = new MatchHistoryWrapper();
            matchHistoryWrapper.setMatchHistory(currentMatchHistory.get());
            matchHistoryWrapper.setPlayerGraphics(mediaPlayerController.getPlayerGraphics());

            if (DataManager.getFilePath() != null) {
                DataManager.saveMatchHistory(matchHistoryWrapper);
            } else {
                saveMatchHistory(matchHistoryWrapper);
            }
        } catch (JAXBException e) {
            showError("Błąd zapisu pliku historii meczu", "Błąd historii meczu", "Podczas zapisywania pliku historii meczu wystąpił błąd.");
        }
    }

    @FXML
    private void saveAsMatchHistory() {
        try {
            MatchHistoryWrapper matchHistoryWrapper = new MatchHistoryWrapper();
            matchHistoryWrapper.setMatchHistory(currentMatchHistory.get());
            matchHistoryWrapper.setPlayerGraphics(mediaPlayerController.getPlayerGraphics());

            saveMatchHistory(matchHistoryWrapper);
        } catch (JAXBException e) {
            showError("Błąd zapisu pliku historii meczu", "Błąd historii meczu", "Podczas zapisywania pliku historii meczu wystąpił błąd.");
        }
    }

    private void saveMatchHistory(MatchHistoryWrapper matchHistoryWrapper) throws JAXBException {
        FileChooser.ExtensionFilter xmlFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Zapis pliku historii meczu");
        fileChooser.getExtensionFilters().add(xmlFilter);

        File file = fileChooser.showSaveDialog(stage);
        if (file != null) {
            if (!file.getPath().endsWith(".xml")) {
                file = new File(file.getPath() + ".xml");
            }

            DataManager.saveMatchHistory(file, matchHistoryWrapper);
            getStage().setTitle(currentMatchHistory.get().getHostTeam().getName() + " "
                    + currentMatchHistory.get().getGuestTeam().getName() + " ("
                    + file.getPath() + ") - " + Main.APP_TITLE);
        }
    }

    @FXML
    private void loadMatchHistory() {
        FileChooser.ExtensionFilter xmlFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Otwórz plik historii meczu");
        fileChooser.getExtensionFilters().add(xmlFilter);

        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            try {
                MatchHistoryWrapper matchHistoryWrapper = DataManager.loadMatchHistory(file);
                currentMatchHistory.set(matchHistoryWrapper.getMatchHistory());
                mediaPlayerController.setPlayerGraphics(matchHistoryWrapper.getPlayerGraphics());

                getStage().setTitle(currentMatchHistory.get().getHostTeam().getName() + " "
                        + currentMatchHistory.get().getGuestTeam().getName() + " ("
                        + file.getPath() + ") - " + Main.APP_TITLE);
            } catch (JAXBException e) {
                showError("Błąd ładowania pliku historii meczu", "Błąd historii meczu", "Podczas ładowania pliku historii meczu wystąpił błąd.");
            }
        }
    }

    public void showError(String title, String headerText, String contentText) {
        Alert alertDialog = new Alert(Alert.AlertType.ERROR);
        alertDialog.setTitle(title);
        alertDialog.getDialogPane().setHeaderText(headerText);
        alertDialog.getDialogPane().setContentText(contentText);
        alertDialog.initModality(Modality.WINDOW_MODAL);
        alertDialog.initOwner(stage);
        alertDialog.showAndWait();
    }

    public Optional<ButtonType> showConfirmation(String title, String headerText, String contentText) {
        Alert alertDialog = new Alert(Alert.AlertType.CONFIRMATION);
        alertDialog.setTitle(title);
        alertDialog.getDialogPane().setHeaderText(headerText);
        alertDialog.getDialogPane().setContentText(contentText);
        alertDialog.initModality(Modality.WINDOW_MODAL);
        alertDialog.initOwner(stage);
        return alertDialog.showAndWait();
    }

    public void showWarning(String title, String headerText, String contentText) {
        Alert alertDialog = new Alert(Alert.AlertType.WARNING);
        alertDialog.setTitle(title);
        alertDialog.getDialogPane().setHeaderText(headerText);
        alertDialog.getDialogPane().setContentText(contentText);
        alertDialog.initModality(Modality.WINDOW_MODAL);
        alertDialog.initOwner(stage);
        alertDialog.showAndWait();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Stage getStage() {
        return stage;
    }

    public ObservableList<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        if (teams != null) {
            this.teams.setAll(teams);
        }
    }

    public ToggleGroup getHalfsMenu() {
        return halfsMenu;
    }

    public ObjectProperty<MatchHistory> currentMatchHistoryProperty() {
        return currentMatchHistory;
    }
}
