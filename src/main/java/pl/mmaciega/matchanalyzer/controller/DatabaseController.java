package pl.mmaciega.matchanalyzer.controller;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import org.controlsfx.control.textfield.CustomTextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.mmaciega.matchanalyzer.Main;
import pl.mmaciega.matchanalyzer.model.*;
import pl.mmaciega.matchanalyzer.ui.CreatePlayerDialog;
import pl.mmaciega.matchanalyzer.ui.EditPlayerDialog;
import pl.mmaciega.matchanalyzer.ui.SelectKeyComboBoxListener;
import pl.mmaciega.matchanalyzer.util.jaxb.wrapers.CountriesListWrapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DatabaseController {
    public static final String COUNTRIES_FLAGS_DIRECTORY = "flags_iso/24/";
    private static final int FIT_LOGOVIEW_SIZE = 150;
    private static final int FIT_LOGO_FILE_SIZE = 300;

    private final Logger logger = LoggerFactory.getLogger(DatabaseController.class);

    @FXML
    private CustomTextField searchTextField;
    @FXML
    private FontAwesomeIconView searchIcon;
    @FXML
    private TreeView teamsTree;
    @FXML
    private TableView<Player> playersTable;
    @FXML
    private TableColumn<Player, Number> numberColumn;
    @FXML
    private TableColumn<Player, Country> countryColumn;
    @FXML
    private TableColumn<Player, Position> positionColumn;
    @FXML
    private TableColumn<Player, String> nameColumn;
    @FXML
    private TextField selectedTeamName;
    @FXML
    private GridPane teamDetails;
    @FXML
    private Label labelInsteadTeamDetails;
    @FXML
    private ImageView logoView;
    @FXML
    private StackPane defaultLogoView;

    private MainController mainController;
    private ObservableList<Team> teams;
    private SortedList<TreeItem> sortedChildren;
    private Team selectedTeam;


    @FXML
    private void initialize() {
        initializeLeftSideOfView();
        initializeRightSideOfView();
    }

    private void initializeLeftSideOfView() {
        teamsTree.setCellFactory(param -> new TeamTreeCell());
        TreeItem rootItem = new TreeItem("Lista drużyn");
        rootItem.setExpanded(true);
        teamsTree.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            TreeItem oldItem = (TreeItem) oldValue;
            TreeItem newItem = (TreeItem) newValue;

            if (oldItem != null && oldItem.getValue() instanceof Team) {
                Team oldTeam = (Team) oldItem.getValue();
                selectedTeamName.textProperty().unbindBidirectional(oldTeam.nameProperty());
                logoView.imageProperty().unbindBidirectional(oldTeam.imageProperty());
            }

            if (newItem.getValue() instanceof Team) {
                Team newTeam = (Team) newItem.getValue();
                selectedTeam = newTeam;
                selectedTeamName.textProperty().bindBidirectional(newTeam.nameProperty());
                logoView.imageProperty().bindBidirectional(newTeam.imageProperty());
                playersTable.setItems(newTeam.getPlayers());
            }
        });
        teamsTree.setRoot(rootItem);
        teamsTree.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.DELETE) {
                deleteTeam();
            }
        });

        searchTextField.setVisible(false);
        searchTextField.setLeft(searchIcon);
    }

    private void initializeRightSideOfView() {
        teamDetails.visibleProperty().bind(Bindings.createBooleanBinding(() -> {
            TreeItem treeItem = (TreeItem) teamsTree.getSelectionModel().getSelectedItem();
            return treeItem != null && treeItem.getValue() instanceof Team;
        }, teamsTree.getSelectionModel().selectedItemProperty()));
        labelInsteadTeamDetails.visibleProperty().bind(teamDetails.visibleProperty().not());

        defaultLogoView.visibleProperty().bind(logoView.imageProperty().isNull());
        logoView.imageProperty().addListener((ObservableValue<? extends Image> observable, Image oldValue, Image newValue) -> {
            if (newValue != null) {
                if (newValue.getWidth() <= FIT_LOGOVIEW_SIZE && newValue.getHeight() <= FIT_LOGOVIEW_SIZE) {
                    logoView.setFitHeight(0);
                    logoView.setFitWidth(0);
                } else {
                    logoView.setFitHeight(FIT_LOGOVIEW_SIZE);
                    logoView.setFitWidth(FIT_LOGOVIEW_SIZE);
                }
            }
        });
        logoView.setOnDragOver(event -> event.acceptTransferModes(TransferMode.LINK));
        logoView.setOnDragDropped(event -> {
            boolean success = false;
            Image image = new Image(event.getDragboard().getUrl());
            if (!image.isError()) {
                logoView.setImage(image);
                success = true;
            } else {
                success = false;
                logger.warn("Błąd wczytywania logo drużyny. Nie wybrano poprawnego pliku graficznego z użyciem metody drag and drop", image.getException());
            }

            event.setDropCompleted(success);
            event.consume();
        });

        playersTable.setPlaceholder(new Label("Brak zawodników w drużynie"));
        numberColumn.setCellValueFactory(cellData -> cellData.getValue().numberProperty());
        numberColumn.setCellFactory(param -> new TableCell<Player, Number>() {
            {
                setAlignment(Pos.CENTER);
            }

            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    setText(item.toString());
                }
            }
        });
        countryColumn.setCellValueFactory(cellData -> cellData.getValue().countryProperty());
        countryColumn.setCellFactory(param -> new TableCell<Player, Country>() {
            {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setAlignment(Pos.CENTER);
            }

            @Override
            protected void updateItem(Country item, boolean empty) {
                super.updateItem(item, empty);

                if (item != null && !empty) {
                    ImageView imageView = new ImageView();
                    imageView.setFitHeight(20);
                    imageView.setFitWidth(20);
                    imageView.setImage(item.getImageFlag());
                    setGraphic(imageView);
                    setTooltip(new Tooltip(item.getNamePl()));
                } else {
                    setGraphic(null);
                    setTooltip(null);
                }
            }
        });
        positionColumn.setCellValueFactory(cellData -> cellData.getValue().positionProperty());
        positionColumn.setCellFactory(new Callback<TableColumn<Player, Position>, TableCell<Player, Position>>() {
            @Override
            public TableCell<Player, Position> call(TableColumn<Player, Position> param) {
                return new TableCell<Player, Position>() {
                    {
                        setAlignment(Pos.CENTER);
                    }

                    @Override
                    protected void updateItem(Position item, boolean empty) {
                        super.updateItem(item, empty);

                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                            setTooltip(null);
                        } else {
                            setText(item.getCodePl());
                            setTooltip(new Tooltip(item.getNamePl()));
                        }
                    }
                };
            }
        });
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
    }

    public void init(MainController mainController) {
        this.mainController = mainController;

        teams = mainController.getTeams();
        teams.addListener((ListChangeListener<? super Team>) c -> {
            while (c.next()) {
                for (Object remTeam : c.getRemoved()) {
                    removeTeamAndCountryIfEmptyFromTree((Team) remTeam);
                }

                for (Object addTeam : c.getAddedSubList()) {
                    addTeamToTree((Team) addTeam);
                    teamsTree.getRoot().getChildren().setAll(sortedChildren);
                }
            }
        });

        sortTree();
    }

    private void removeTeamAndCountryIfEmptyFromTree(Team team) {
        ObservableList<TreeItem> countryTreeItems = teamsTree.getRoot().getChildren();
        countryTreeItems.stream()
                .filter(treeItem -> treeItem.getValue().equals(team.getCountry()))
                .findFirst()
                .ifPresent(countryTreeItem -> {
                    countryTreeItem.getChildren().removeIf(o -> ((TreeItem<Team>) o).getValue() == team);
                    if (countryTreeItem.getChildren().isEmpty()) {
                        countryTreeItem.getParent().getChildren().remove(countryTreeItem);
                    }
                });
    }

    private void addTeamToTree(Team team) {
        TreeItem<Team> teamNode = new TreeItem<>(team);
        ObservableList<TreeItem> children = teamsTree.getRoot().getChildren();
        Optional<TreeItem> countryTreeItem = children.stream()
                .filter(treeItem -> treeItem.getValue().equals(team.getCountry()))
                .findFirst();

        if (countryTreeItem.isPresent()) {
            countryTreeItem.get().getChildren().add(teamNode);
            countryTreeItem.get().getChildren().sort((Object o1, Object o2) -> {
                TreeItem<Team> t1 = (TreeItem<Team>) o1;
                TreeItem<Team> t2 = (TreeItem<Team>) o2;

                return t1.getValue().getName().compareToIgnoreCase(t2.getValue().getName());
            });
        } else {
            TreeItem countryNode = new TreeItem(team.getCountry());
            teamsTree.getRoot().getChildren().add(countryNode);
            countryNode.getChildren().add(teamNode);
        }

        teamsTree.requestFocus();
    }

    private void sortTree() {
        sortedChildren = new SortedList<TreeItem>(teamsTree.getRoot().getChildren(),
                (o1, o2) -> ((Country) o1.getValue()).getNamePl().compareToIgnoreCase(((Country) o2.getValue()).getNamePl()));
        teamsTree.getRoot().getChildren().setAll(sortedChildren);
        teamsTree.getRoot().getChildren().forEach(o -> ((TreeItem) o).setExpanded(false));
        teamsTree.getSelectionModel().select(0);
    }

    @FXML
    private void removeSelectedPlayer() {
        Player selectedPlayer = playersTable.getSelectionModel().getSelectedItem();
        if (selectedPlayer != null) {
            selectedTeam.getPlayers().remove(selectedPlayer);
        } else {
            mainController.showWarning("Błąd usuwania zawodnika", "Błąd bazy zawodników", "Wybierz zawodnika z tabeli aby go usunać.");
            logger.warn("Nie wybrano zawodnika do usunięcia");
        }
    }

    @FXML
    private void createTeam() {
        try {
            Dialog<Team> dialog = new CreateTeamDialog();
            dialog.initOwner(mainController.getStage());
            Optional<Team> result = dialog.showAndWait();
            result.ifPresent(teams::add);
        } catch (IOException e) {
            mainController.showError("Błąd otwierania okna", "Błąd okna dodaj drużynę", "Wystąpił błąd podczas otwierania okna pozwalającego na stworzenie nowej drużyny.");
            logger.error("Wystapił błąd podczas otwierania okna tworzenia nowej drużyny", e);
        }
    }

    @FXML
    private void deleteTeam() {
        TreeItem item = (TreeItem) teamsTree.getSelectionModel().getSelectedItem();

        if (item.getValue() instanceof Team) {
            teams.remove(item.getValue());
        } else {
            mainController.showWarning("Błąd usuwania klubu", "Błąd bazy zespołów", "Wybierz drużynę w celu usunięcia jej z bazy.");
            logger.warn("Nie wybrano zespołu do usunięcia");
        }
    }

    @FXML
    private void createPlayer() {
        try {
            Dialog<Player> dialog = new CreatePlayerDialog(selectedTeam);
            dialog.initOwner(mainController.getStage());
            Optional<Player> result = dialog.showAndWait();
            result.ifPresent(newPlayer -> selectedTeam.getPlayers().add(newPlayer));
        } catch (IOException e) {
            mainController.showError("Błąd otwierania okna", "Błąd okna nowy zawodnik", "Wystąpił błąd podczas otwierania okna pozwalającego na stworzenie nowego zawodnika.");
            logger.error("Wyśtapił błąd podczas otwierania okna tworzenia nowego zawodnika", e);
        }
    }

    @FXML
    private void editPlayer() {
        Player selectedPlayer = playersTable.getSelectionModel().getSelectedItem();

        if (selectedPlayer != null) {
            try {
                Dialog<Player> dialog = new EditPlayerDialog(selectedPlayer);
                Optional<Player> result = dialog.showAndWait();
                result.ifPresent(newPlayer -> {
                    selectedPlayer.setCountry(newPlayer.getCountry());
                    selectedPlayer.setName(newPlayer.getName());
                    selectedPlayer.setNumber(newPlayer.getNumber());
                    selectedPlayer.setPosition(newPlayer.getPosition());
                });
            } catch (IOException e) {
                mainController.showError("Błąd otwierania okna", "Błąd okna edytuj zawodnika", "Wystąpił błąd podczas otwierania okna pozwalającego na edytowanie zawodnika.");
                logger.error("Wyśtapił błąd podczas otwierania okna edytowania nowego zawodnika", e);
            }
        } else {
            mainController.showWarning("Błąd modyfikacji zawodnika", "Błąd bazy zawodników", "Wybierz zawodnika z tabeli aby go zmodyfikować.");
            logger.warn("Nie wybrano zawodnika do modyfikacji");
        }
    }

    @FXML
    private void selectLogoForSelectedTeam() {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter
        List<String> extensionsList = new ArrayList<>();
        extensionsList.add("*.jpg");
        extensionsList.add("*.png");
        extensionsList.add("*.gif");
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter(
                "Images files (*.jpg | *.png | *.gif)", extensionsList);
        fileChooser.getExtensionFilters().addAll(extensionFilter);

        File file = fileChooser.showOpenDialog(mainController.getStage());
        if (file != null) {
            try {
                Image image = new Image("file:/" + file.getAbsolutePath(), FIT_LOGO_FILE_SIZE, FIT_LOGO_FILE_SIZE, true, true);
                selectedTeam.setLogoPath("file:/" + file.getAbsolutePath());
                logoView.setImage(image);
            } catch (IllegalArgumentException e) {
                mainController.showError("Błąd wczytywania logo drużyny", "", "Nie wybrano poprawnego pliku graficznego.");
                logger.warn("Błąd wczytywania logo drużyny. Nie wybrano poprawnego pliku graficznego", e);
            }

        }
    }

    @FXML
    private void removeLogoForSelectedTeam() {
        selectedTeam.setImage(null);
    }

    private final class CreateTeamDialog extends Dialog<Team> {
        @FXML
        private TextField teamName;
        @FXML
        private ComboBox<Country> countryComboBox;

        public CreateTeamDialog() throws IOException {
            super();

            setTitle("Stwórz nową drużyne");
            setHeaderText("Wprowadz podstawowe dane na temat nowej drużyny");

            GridPane dialogPane = loadDialogPane();

            ButtonType createButtonType = new ButtonType("Dodaj", ButtonBar.ButtonData.OK_DONE);
            ButtonType cancelButtonType = new ButtonType("Anuluj", ButtonBar.ButtonData.CANCEL_CLOSE);
            getDialogPane().getButtonTypes().addAll(createButtonType, cancelButtonType);
            getDialogPane().setContent(dialogPane);

            Node createButton = getDialogPane().lookupButton(createButtonType);
            BooleanBinding booleanBinding = Bindings.createBooleanBinding(() -> {
                Optional<Team> teamOptional = teams.stream().filter(team -> team.getName().equalsIgnoreCase(teamName.getText())).findFirst();
                return teamOptional.isPresent() ? true : false;
            }, teamName.textProperty());
            createButton.disableProperty().bind(teamName.textProperty().isEmpty().or(booleanBinding.or(countryComboBox.valueProperty().isNull())));

            Platform.runLater(teamName::requestFocus);

            setResultConverter(dialogButton -> {
                if (dialogButton == createButtonType) {
                    return new Team(teamName.getText(), countryComboBox.getSelectionModel().getSelectedItem());
                }
                return null;
            });
        }

        private GridPane loadDialogPane() throws IOException {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(Main.class.getResource("/view/edit/CreateTeam.fxml"));
            fxmlLoader.setController(this);
            return fxmlLoader.load();
        }

        @FXML
        private void initialize() {
            countryComboBox.setCellFactory(new Callback<ListView<Country>, ListCell<Country>>() {
                @Override
                public ListCell<Country> call(ListView<Country> param) {
                    return new ListCell<Country>() {
                        @Override
                        protected void updateItem(Country item, boolean empty) {
                            super.updateItem(item, empty);

                            if (empty || item == null) {
                                setText(null);
                                setGraphic(null);
                            } else {
                                setText(item.getNamePl());
                                setGraphic(new ImageView(item.getImageFlag()));
                            }
                        }
                    };
                }
            });
            countryComboBox.setButtonCell(new ListCell() {
                @Override
                protected void updateItem(Object item, boolean empty) {
                    super.updateItem(item, empty);

                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        Country country = (Country) item;

                        setText(country.getNamePl());
                        setGraphic(new ImageView(country.getImageFlag()));
                    }
                }
            });
            countryComboBox.setItems(FXCollections.observableArrayList(CountriesListWrapper.getCountries()));
            new SelectKeyComboBoxListener(countryComboBox);
        }
    }

    private static final class TeamTreeCell extends TreeCell {
        @Override
        protected void updateItem(Object item, boolean empty) {
            super.updateItem(item, empty);
            textProperty().unbind();

            if (empty || item == null) {
                setText(null);
                setGraphic(null);
            } else {
                if (item instanceof String) {
                    setText((String) item);
                    setGraphic(null);
                } else if (item instanceof Country) {
                    Country country = (Country) item;
                    textProperty().bind(country.namePlProperty());
                    setGraphic(new ImageView(country.getImageFlag()));
                } else if (item instanceof Team) {
                    Team team = (Team) item;
                    textProperty().bind(team.nameProperty());

                    ImageView imageView = new ImageView();
                    imageView.setPreserveRatio(true);
                    imageView.setSmooth(true);
                    imageView.setFitHeight(24);
                    imageView.setFitWidth(24);
                    imageView.imageProperty().bind(team.imageProperty());
                    setGraphic(imageView);
                }
            }
        }
    }
}
