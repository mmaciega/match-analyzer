package pl.mmaciega.matchanalyzer.controller;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.mmaciega.matchanalyzer.model.MatchHistory;
import pl.mmaciega.matchanalyzer.model.events.FootballEvent;
import pl.mmaciega.matchanalyzer.ui.ResizableMediaPlayer;
import pl.mmaciega.matchanalyzer.ui.pitch.PlayerGraphic;
import pl.mmaciega.matchanalyzer.ui.pitch.PositionGraphic;
import uk.co.caprica.vlcj.binding.internal.libvlc_media_t;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventListener;
import uk.co.caprica.vlcj.player.direct.DirectMediaPlayer;

import java.util.List;

/**
 * Created by Mytka on 2015-07-16.
 */
public class MediaPlayerController {
    @FXML
    private PitchController pitchController;

    @FXML
    private Pane playerHolder;
    @FXML
    private FontAwesomeIconView playPauseIcon;
    @FXML
    private FontAwesomeIconView muteIcon;
    @FXML
    private Label time;
    @FXML
    private Slider volume;
    @FXML
    private Slider position;
    @FXML
    private Label volumePercent;
    @FXML
    private VBox noFilePanel;
    @FXML
    private SplitPane pitch;
    @FXML
    private SplitPane mainSplit;

    private ResizableMediaPlayer resizableMediaPlayer;
    private MainController mainController;
    private ObjectProperty<MatchHistory> currentMatchHistory = new SimpleObjectProperty<>();
    private final Logger logger = LoggerFactory.getLogger(MediaPlayerController.class);

    @FXML
    private void initialize() {
        resizableMediaPlayer = ResizableMediaPlayer.getInstance();
        resizableMediaPlayer.getMediaPlayer().addMediaPlayerEventListener(new MediaPlayerEventListener() {
            @Override
            public void mediaChanged(MediaPlayer mediaPlayer, libvlc_media_t media, String mrl) {
            }

            @Override
            public void opening(MediaPlayer mediaPlayer) {
            }

            @Override
            public void buffering(MediaPlayer mediaPlayer, float newCache) {

            }

            @Override
            public void playing(MediaPlayer mediaPlayer) {
                Platform.runLater(() -> {
                    playPauseIcon.setGlyphName("PAUSE");
                    position.setDisable(false);
                });
            }

            @Override
            public void paused(MediaPlayer mediaPlayer) {
                Platform.runLater(() -> playPauseIcon.setGlyphName("PLAY"));
            }

            @Override
            public void stopped(MediaPlayer mediaPlayer) {
                Platform.runLater(MediaPlayerController.this::clearUI);
            }

            @Override
            public void forward(MediaPlayer mediaPlayer) {

            }

            @Override
            public void backward(MediaPlayer mediaPlayer) {

            }

            @Override
            public void finished(MediaPlayer mediaPlayer) {

            }

            @Override
            public void timeChanged(MediaPlayer mediaPlayer, long newTime) {

            }

            @Override
            public void positionChanged(MediaPlayer mediaPlayer, float newPosition) {
                Platform.runLater(() -> {
                    long millis = mediaPlayer.getTime();
                    long second = (millis / 1000) % 60;
                    long minute = (millis / (1000 * 60)) % 60;
                    long hour = (millis / (1000 * 60 * 60)) % 24;

                    String formattedTime = String.format("%02d:%02d:%02d", hour, minute, second);
                    time.setText(formattedTime);

                    position.setValue(newPosition * 100.0);
                });
            }

            @Override
            public void seekableChanged(MediaPlayer mediaPlayer, int newSeekable) {

            }

            @Override
            public void pausableChanged(MediaPlayer mediaPlayer, int newPausable) {

            }

            @Override
            public void titleChanged(MediaPlayer mediaPlayer, int newTitle) {

            }

            @Override
            public void snapshotTaken(MediaPlayer mediaPlayer, String filename) {

            }

            @Override
            public void lengthChanged(MediaPlayer mediaPlayer, long newLength) {
            }

            @Override
            public void videoOutput(MediaPlayer mediaPlayer, int newCount) {
                mediaPlayer.setVolume((int) volume.getValue());
                mediaPlayer.mute(muteIcon.isVisible());
            }

            @Override
            public void scrambledChanged(MediaPlayer mediaPlayer, int newScrambled) {

            }

            @Override
            public void elementaryStreamAdded(MediaPlayer mediaPlayer, int type, int id) {

            }

            @Override
            public void elementaryStreamDeleted(MediaPlayer mediaPlayer, int type, int id) {

            }

            @Override
            public void elementaryStreamSelected(MediaPlayer mediaPlayer, int type, int id) {

            }

            @Override
            public void error(MediaPlayer mediaPlayer) {
                Platform.runLater(() -> {
                    clearUI();
                    mainController.showError("Błąd otwarcia filmu", "Błąd odtwarzacza", "Wystąpił problem podczas otwierania pliku multimedialnego.");
                    logger.warn("Błąd otwarcia filmu");
                });
            }

            @Override
            public void mediaMetaChanged(MediaPlayer mediaPlayer, int metaType) {

            }

            @Override
            public void mediaSubItemAdded(MediaPlayer mediaPlayer, libvlc_media_t subItem) {

            }

            @Override
            public void mediaDurationChanged(MediaPlayer mediaPlayer, long newDuration) {

            }

            @Override
            public void mediaParsedChanged(MediaPlayer mediaPlayer, int newStatus) {

            }

            @Override
            public void mediaFreed(MediaPlayer mediaPlayer) {

            }

            @Override
            public void mediaStateChanged(MediaPlayer mediaPlayer, int newState) {

            }

            @Override
            public void mediaSubItemTreeAdded(MediaPlayer mediaPlayer, libvlc_media_t item) {

            }

            @Override
            public void newMedia(MediaPlayer mediaPlayer) {

            }

            @Override
            public void subItemPlayed(MediaPlayer mediaPlayer, int subItemIndex) {

            }

            @Override
            public void subItemFinished(MediaPlayer mediaPlayer, int subItemIndex) {

            }

            @Override
            public void endOfSubItems(MediaPlayer mediaPlayer) {

            }
        });
        resizableMediaPlayer.init(playerHolder);

        pitch.setOpacity(0.0);
        currentMatchHistory.addListener((observable1, oldValue1, newValue1) -> {
            pitch.setOpacity(newValue1 != null ? 1.0 : 0.0);
        });
        noFilePanel.setVisible(false);
        noFilePanel.visibleProperty().bind(pitch.opacityProperty().isEqualTo(0));

        StringBinding percentVolumeBindings = Bindings.createStringBinding(() -> Math.round(volume.getValue()) + "%", volume.valueProperty());
        volumePercent.textProperty().bind(percentVolumeBindings);
        volume.valueProperty().addListener((observable, oldValue, newValue) -> resizableMediaPlayer.getMediaPlayer().setVolume(newValue.intValue()));
        position.valueProperty().addListener(new ChangeListener<Number>() {
            private boolean isSliderValueChangingOrMovieNotPlaying() {
                return position.valueChangingProperty().get() || !resizableMediaPlayer.getMediaPlayer().isPlaying();
            }

            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if (isSliderValueChangingOrMovieNotPlaying()) {
                    resizableMediaPlayer.getMediaPlayer().setPosition(newValue.floatValue() / (float) 100.00);
                }
            }
        });

        muteIcon.setVisible(false);
    }


    private void clearUI() {
        playPauseIcon.setGlyphName("PLAY");
        time.setText("--:--:--");
        position.setValue(0);
        position.setDisable(true);
    }

    @FXML
    private void playPauseMedia() {
        DirectMediaPlayer mediaPlayer = resizableMediaPlayer.getMediaPlayer();

        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        } else {
            mediaPlayer.play();
        }
    }

    private void pauseIfPlayingMedia() {
        DirectMediaPlayer mediaPlayer = resizableMediaPlayer.getMediaPlayer();

        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
    }

    @FXML
    private void stopMedia() {
        resizableMediaPlayer.getMediaPlayer().stop();
    }

    @FXML
    private void muteSound() {
        resizableMediaPlayer.getMediaPlayer().mute();

        if (resizableMediaPlayer.getMediaPlayer().isMute()) {
            muteIcon.setVisible(true);
        } else {
            muteIcon.setVisible(false);
        }
    }

    @FXML
    private void ejectFile() {
        mainController.ejectFile();
    }

    public void init(MainController mainController) {
        this.mainController = mainController;
        this.currentMatchHistory.bind(mainController.currentMatchHistoryProperty());
        this.currentMatchHistory.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                pitchController.setFootballEventsList(newValue.getFootballEvents());
                pitchController.setSubPlayers(FXCollections.observableArrayList(newValue.getSubPlayers()));
                pitchController.setFirstSquadOnPitch(newValue.getFirstSquad());
            }
        });

        pitchController.currentMatchHistoryProperty().bind(mainController.currentMatchHistoryProperty());
        mainController.getHalfsMenu().selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                pitchController.setCurrentMatchHalf((FootballEvent.Halfs) newValue.getUserData());
            }
        });
    }

    public ObservableList<PlayerGraphic> getPlayerGraphics() {
        return pitchController.getPlayerGraphics();
    }
    public void setPlayerGraphics(List<PlayerGraphic> playerGraphics) {
        pitchController.setPlayerGraphics(playerGraphics);
    }

    public List<PositionGraphic> getPositionGraphicList() {
        return pitchController.getPositionGraphicList();
    }
}
