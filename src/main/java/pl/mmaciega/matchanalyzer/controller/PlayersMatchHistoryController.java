package pl.mmaciega.matchanalyzer.controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.mmaciega.matchanalyzer.model.Country;
import pl.mmaciega.matchanalyzer.model.Player;
import pl.mmaciega.matchanalyzer.model.Position;
import pl.mmaciega.matchanalyzer.model.Team;
import pl.mmaciega.matchanalyzer.ui.CreatePlayerDialog;
import pl.mmaciega.matchanalyzer.ui.EditPlayerDialog;

import java.io.IOException;
import java.util.Optional;

/**
 * Created by Mytka on 2015-08-01.
 */
public class PlayersMatchHistoryController {
    @FXML
    private TableView<Player> playersTable;
    @FXML
    private TableColumn<Player, Number> numberColumn;
    @FXML
    private TableColumn<Player, Country> countryColumn;
    @FXML
    private TableColumn<Player, Position> positionColumn;
    @FXML
    private TableColumn<Player, String> nameColumn;
    @FXML
    private Button editPlayerButton;
    @FXML
    private Button removeButton;

    private Team team;
    private ObservableList<Player> subPlayers;
    private Stage stage;

    private final Logger logger = LoggerFactory.getLogger(PlayersMatchHistoryController.class);
    private static final Tooltip TOOLTIP_WARNING = new Tooltip("Zawodnik na boisku. Zmień go aby usunąć!");

    @FXML
    private void initialize() {
        editPlayerButton.disableProperty().bind(playersTable.getSelectionModel().selectedItemProperty().isNull());
        TOOLTIP_WARNING.setAutoFix(true);
        TOOLTIP_WARNING.setAutoHide(true);


        playersTable.setPlaceholder(new Label("Brak zawodników w drużynie"));
        numberColumn.setCellValueFactory(cellData -> cellData.getValue().numberProperty());
        numberColumn.setCellFactory(param -> new TableCell<Player, Number>() {
            {
                setAlignment(Pos.CENTER);
            }

            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    setText(item.toString());
                }
            }
        });
        countryColumn.setCellValueFactory(cellData -> cellData.getValue().countryProperty());
        countryColumn.setCellFactory(param -> new TableCell<Player, Country>() {
            {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setAlignment(Pos.CENTER);
            }

            @Override
            protected void updateItem(Country item, boolean empty) {
                super.updateItem(item, empty);

                if (item != null && !empty) {
                    ImageView imageView = new ImageView();
                    imageView.setFitHeight(20);
                    imageView.setFitWidth(20);
                    imageView.setImage(item.getImageFlag());
                    setGraphic(imageView);
                    setTooltip(new Tooltip(item.getNamePl()));
                } else {
                    setGraphic(null);
                    setTooltip(null);
                }
            }
        });
        positionColumn.setCellValueFactory(cellData -> cellData.getValue().positionProperty());
        positionColumn.setCellFactory(new Callback<TableColumn<Player, Position>, TableCell<Player, Position>>() {
            @Override
            public TableCell<Player, Position> call(TableColumn<Player, Position> param) {
                return new TableCell<Player, Position>() {
                    {
                        setAlignment(Pos.CENTER);
                    }

                    @Override
                    protected void updateItem(Position item, boolean empty) {
                        super.updateItem(item, empty);

                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                            setTooltip(null);
                        } else {
                            setText(item.getCodePl());
                            setTooltip(new Tooltip(item.getNamePl()));
                        }
                    }
                };
            }
        });
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());

    }

    @FXML
    private void createPlayer() {
        try {
            Dialog<Player> dialog = new CreatePlayerDialog(team);
            Optional<Player> result = dialog.showAndWait();
            result.ifPresent(newPlayer -> {
                team.getPlayers().add(newPlayer);
                subPlayers.add(newPlayer);
            });
        } catch (IOException e) {
            logger.error("Wystapił błąd podczas otwierania okna tworzenia nowego zawodnika", e);
        }
    }

    @FXML
    private void editPlayer() {
        Player selectedPlayer = playersTable.getSelectionModel().getSelectedItem();

        try {
            Dialog<Player> dialog = new EditPlayerDialog(selectedPlayer);
            Optional<Player> result = dialog.showAndWait();
            result.ifPresent(newPlayer -> {
                selectedPlayer.setCountry(newPlayer.getCountry());
                selectedPlayer.setName(newPlayer.getName());
                selectedPlayer.setNumber(newPlayer.getNumber());
                selectedPlayer.setPosition(newPlayer.getPosition());
            });
        } catch (IOException e) {
            logger.error("Wystapił błąd podczas otwierania okna edytowania nowego zawodnika", e);
        }
    }

    @FXML
    private void removeSelectedPlayer() {
        Player selectedPlayer = playersTable.getSelectionModel().getSelectedItem();
        if (subPlayers.contains(selectedPlayer)) {
            team.getPlayers().remove(selectedPlayer);
            subPlayers.remove(selectedPlayer);
        } else {
            Point2D point2D = removeButton.localToScreen(removeButton.getWidth() - 30, removeButton.getHeight() / 2);
            TOOLTIP_WARNING.show(removeButton, point2D.getX(), point2D.getY());
            System.out.println("Zawodnik na boisku");
        }
    }


    public void setTeam(Team team) {
        this.team = team;
        playersTable.setItems(team.getPlayers());
    }

    public void setSubPlayers(ObservableList<Player> subPlayers) {
        this.subPlayers = subPlayers;
    }
}

