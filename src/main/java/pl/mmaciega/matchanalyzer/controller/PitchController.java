package pl.mmaciega.matchanalyzer.controller;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import pl.mmaciega.matchanalyzer.Main;
import pl.mmaciega.matchanalyzer.model.*;
import pl.mmaciega.matchanalyzer.model.events.FootballEvent;
import pl.mmaciega.matchanalyzer.ui.ResizableMediaPlayer;
import pl.mmaciega.matchanalyzer.ui.pitch.FootballEventContextMenu;
import pl.mmaciega.matchanalyzer.ui.pitch.PlayerGraphic;
import pl.mmaciega.matchanalyzer.ui.pitch.PositionGraphic;
import pl.mmaciega.matchanalyzer.util.ResourceFileManager;
import uk.co.caprica.vlcj.player.direct.DirectMediaPlayer;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PitchController {
    private static final Image DEFAULT_LOGO = ResourceFileManager.getImage("defaultTeamLogo.png");

    @FXML
    private AnchorPane pitchPanel;
    @FXML
    private ImageView pitchView;
    @FXML
    private StackPane pitchStack;
    @FXML
    private GridPane pitchGrid;
    @FXML
    private TableView<FootballEvent> eventsTable;
    @FXML
    private TableColumn<FootballEvent, Boolean> editColumn;
    @FXML
    private TableColumn<FootballEvent, FootballEvent.Halfs> halfColumn;
    @FXML
    private TableColumn<FootballEvent, FootballEvent.Types> eventTypeColumn;
    @FXML
    private TableColumn<FootballEvent, Player> playerColumn;
    @FXML
    private TableColumn<FootballEvent, Boolean> successfulColumn;
    @FXML
    private Label hostTextField;
    @FXML
    private ImageView hostImageView;
    @FXML
    private Label guestTextField;
    @FXML
    private ImageView guestImageView;
    @FXML
    private Label matchDateTextField;
    @FXML
    private TableView<Player> subPlayersTable;
    @FXML
    private TableColumn<Player, Number> numberColumn;
    @FXML
    private TableColumn<Player, Country> countryColumn;
    @FXML
    private TableColumn<Player, Position> positionColumn;
    @FXML
    private TableColumn<Player, String> nameColumn;

    private PlayerGraphic currentDraggingNewPlayer;
    @FXML
    private PlayerGraphic draggingPlayerGraphic;
    @FXML
    private ImageView draggingBall;

    private Node draggingNode;

    @FXML
    private StackPane hostStack;
    @FXML
    private StackPane guestStack;


    private FootballEvent.Halfs currentMatchHalf = FootballEvent.Halfs.FIRST;

    private FootballEventContextMenu footballEventContextMenu = new FootballEventContextMenu();

    private static List<PositionGraphic> positionGraphics = new ArrayList<>();
    private ObservableList<PlayerGraphic> playerGraphics = FXCollections.observableArrayList();

    private DoubleProperty pitchScaleX, pitchScaleY;
    private double startRelocateDraggingX, startRelocateDraggingY;

    private ObjectProperty<MatchHistory> currentMatchHistory = new SimpleObjectProperty<>();
    private boolean playerWasRunning = false;

    @FXML
    private void initialize() {
        initializePitchViewFitSize();
        initializeScalesProperties();

        PlayerGraphic.setFootballEventContextMenu(footballEventContextMenu);

        pitchGrid.getChildren().addListener((ListChangeListener<? super Node>) c -> {
            while (c.next()) {
                if (c.wasAdded()) {
                    for (Object o : c.getAddedSubList()) {
                        Node node = (Node) o;
                        node.scaleXProperty().bind(pitchScaleX);
                        node.scaleYProperty().bind(pitchScaleY);
                    }
                }
            }
        });

        pitchGrid.getChildren().forEach(node -> {
            if (node instanceof PositionGraphic) {
                positionGraphics.add((PositionGraphic) node);
            }

            node.scaleXProperty().bind(pitchScaleX);
            node.scaleYProperty().bind(pitchScaleY);
        });
        positionGraphics.forEach(positionGraphic -> positionGraphic.setVisible(false));

        eventsTable.setPlaceholder(new Label("Brak zarejestrowanych zdarzeń"));
        footballEventContextMenu.setOnHiding(event -> {
            FootballEvent newFootballEvent = footballEventContextMenu.getResult();
            if (newFootballEvent != null) {
                newFootballEvent.setHalf(currentMatchHalf);
                eventsTable.getItems().add(newFootballEvent);
            }
        });

        playerGraphics.addListener((ListChangeListener<PlayerGraphic>) c -> {
            while (c.next()) {
                if (c.wasAdded() || c.wasRemoved()) {
                    for (PlayerGraphic additem : c.<PlayerGraphic>getAddedSubList()) {
                        GridPane.setConstraints(additem,
                                GridPane.getColumnIndex(additem.getPositionGraphic()),
                                GridPane.getRowIndex(additem.getPositionGraphic()),
                                1, 1);
                        pitchGrid.getChildren().add(additem);
                    }
                    for (PlayerGraphic remitem : c.<PlayerGraphic>getRemoved()) {
                        pitchGrid.getChildren().remove(remitem);
                    }
                }
            }

        });

        subPlayersTable.setPlaceholder(new Label("Brak zawodników do zmiany w drużynie"));
        numberColumn.setCellValueFactory(cellData -> cellData.getValue().numberProperty());
        numberColumn.setCellFactory(param -> new TableCell<Player, Number>() {
            {
                setAlignment(Pos.CENTER);
                setOnDragDetected(createDragDetectedHandler(this));
            }

            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    setText(item.toString());
                }
            }
        });
        countryColumn.setCellValueFactory(cellData -> cellData.getValue().countryProperty());
        countryColumn.setCellFactory(param -> new TableCell<Player, Country>() {
            {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setAlignment(Pos.CENTER);
                setOnDragDetected(createDragDetectedHandler(this));
            }

            @Override
            protected void updateItem(Country item, boolean empty) {
                super.updateItem(item, empty);

                if (item != null && !empty) {
                    ImageView imageView = new ImageView();
                    imageView.setFitHeight(20);
                    imageView.setFitWidth(20);
                    imageView.setImage(item.getImageFlag());
                    setGraphic(imageView);
                    setTooltip(new Tooltip(item.getNamePl()));
                } else {
                    setGraphic(null);
                    setTooltip(null);
                }
            }
        });
        positionColumn.setCellValueFactory(cellData -> cellData.getValue().positionProperty());
        positionColumn.setCellFactory(new Callback<TableColumn<Player, Position>, TableCell<Player, Position>>() {
            @Override
            public TableCell<Player, Position> call(TableColumn<Player, Position> param) {
                return new TableCell<Player, Position>() {
                    {
                        setAlignment(Pos.CENTER);
                        setOnDragDetected(createDragDetectedHandler(this));
                    }

                    @Override
                    protected void updateItem(Position item, boolean empty) {
                        super.updateItem(item, empty);

                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                            setTooltip(null);
                        } else {
                            setText(item.getCodePl());
                            setTooltip(new Tooltip(item.getNamePl()));
                        }
                    }
                };
            }
        });
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        nameColumn.setCellFactory(param -> new TableCell<Player, String>() {
            {
                setOnDragDetected(createDragDetectedHandler(this));
            }

            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    setText(item.toString());
                }
            }
        });

        pitchPanel.getChildren().remove(draggingPlayerGraphic);
        draggingPlayerGraphic.scaleXProperty().bind(pitchScaleX);
        draggingPlayerGraphic.scaleYProperty().bind(pitchScaleY);

        pitchPanel.getChildren().remove(draggingBall);
        draggingBall.scaleXProperty().bind(pitchScaleX);
        draggingBall.scaleYProperty().bind(pitchScaleY);

        editColumn.setCellValueFactory(param -> new SimpleBooleanProperty(param.getValue() != null));
        editColumn.setCellFactory(param1 -> new TableCell<FootballEvent, Boolean>() {
            FontAwesomeIconView fontAwesomeIconView;
            Tooltip tooltip;

            {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setAlignment(Pos.CENTER);

                fontAwesomeIconView = new FontAwesomeIconView(FontAwesomeIcon.REMOVE);
                fontAwesomeIconView.setFill(Color.DARKRED);
                fontAwesomeIconView.setStroke(Color.BLACK);
                fontAwesomeIconView.setStrokeWidth(1.0);
                fontAwesomeIconView.setOnMouseClicked(event -> {
                    if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                        FootballEvent currentFootballEvent = this.getTableView().getItems().get(this.getIndex());
                        getTableView().getItems().remove(currentFootballEvent);
                        getTableView().getSelectionModel().clearSelection();
                    }
                });

                tooltip = new Tooltip("Usuń zdarzenie");
            }


            @Override
            protected void updateItem(Boolean item, boolean empty) {
                super.updateItem(item, empty);

                if (!empty) {
                    setGraphic(fontAwesomeIconView);
                    setTooltip(tooltip);
                } else {
                    setGraphic(null);
                    setTooltip(null);
                }
            }
        });
        halfColumn.setCellValueFactory(cellData -> cellData.getValue().halfProperty());
        halfColumn.setCellFactory(param -> new TableCell<FootballEvent, FootballEvent.Halfs>() {
            {
                setAlignment(Pos.CENTER);
            }

            @Override
            protected void updateItem(FootballEvent.Halfs item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    setText(item.getValue());
                    setGraphic(null);
                }
            }
        });
        eventTypeColumn.setCellValueFactory(cellData -> cellData.getValue().typeProperty());
        eventTypeColumn.setCellFactory(param -> new TableCell<FootballEvent, FootballEvent.Types>() {
            @Override
            protected void updateItem(FootballEvent.Types item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    setText(item.getName());
                    setGraphic(null);
                }
            }
        });
        playerColumn.setCellValueFactory(cellData -> cellData.getValue().sourcePlayerProperty());
        playerColumn.setCellFactory(param -> new TableCell<FootballEvent, Player>() {
            @Override
            protected void updateItem(Player item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    setText(item.getNumber() + " " + item.getName());
                    setGraphic(null);
                }
            }
        });
        successfulColumn.setCellValueFactory(cellData -> cellData.getValue().successfulProperty());
        successfulColumn.setCellFactory(CheckBoxTableCell.forTableColumn(successfulColumn));

        currentMatchHistory.addListener((observable, oldValue, newValue) -> {
                    if (oldValue != null) {
                        hostTextField.textProperty().unbindBidirectional(oldValue.getHostTeam().nameProperty());
                        hostTextField.underlineProperty().unbind();
                        hostImageView.imageProperty().unbind();
                        guestTextField.textProperty().unbindBidirectional(oldValue.getGuestTeam().nameProperty());
                        guestTextField.underlineProperty().unbind();
                        guestImageView.imageProperty().unbind();
                        matchDateTextField.textProperty().unbind();
                    }

                    if (newValue != null) {
                        hostTextField.textProperty().bindBidirectional(newValue.getHostTeam().nameProperty());
                        hostTextField.underlineProperty().bind(Bindings.when(new SimpleBooleanProperty(newValue.getSourceTeam().equals(newValue.getHostTeam()))).then(true).otherwise(false));
                        hostImageView.imageProperty().bind(Bindings.createObjectBinding(() -> newValue.getHostTeam().getImage() != null
                                ? newValue.getHostTeam().getImage()
                                : DEFAULT_LOGO, newValue.getHostTeam().imageProperty()));
                        guestTextField.textProperty().bindBidirectional(newValue.getGuestTeam().nameProperty());
                        guestTextField.underlineProperty().bind(Bindings.when(new SimpleBooleanProperty(newValue.getSourceTeam().equals(newValue.getGuestTeam()))).then(true).otherwise(false));
                        guestImageView.imageProperty().bind(Bindings.createObjectBinding(() -> newValue.getGuestTeam().getImage() != null
                                ? newValue.getGuestTeam().getImage()
                                : DEFAULT_LOGO, newValue.getGuestTeam().imageProperty()));
                        matchDateTextField.textProperty().bind(Bindings.createStringBinding(() -> {
                            if (newValue.getMatchDate() != null) {
                                return newValue.getMatchDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
                            } else {
                                return "";
                            }
                        }, newValue.matchDateProperty()));
                    }
                }
        );

        initalizeDragAndDrop();
    }

    private void initializePitchViewFitSize() {
        pitchView.fitHeightProperty().bind(pitchPanel.heightProperty());
        pitchView.fitWidthProperty().bind(pitchPanel.widthProperty());

        hostImageView.fitWidthProperty().bindBidirectional(hostImageView.fitHeightProperty());
        guestImageView.fitWidthProperty().bindBidirectional(guestImageView.fitHeightProperty());

        hostStack.widthProperty().addListener((observable, oldValue, newValue) -> {
            Platform.runLater(() -> hostImageView.setFitWidth(newValue.doubleValue()));
        });
        guestStack.widthProperty().addListener((observable, oldValue, newValue) -> {
            Platform.runLater(() -> guestImageView.setFitWidth(newValue.doubleValue()));
        });
    }

    private void initializeScalesProperties() {
        DoubleBinding scaleXBinding = Bindings.createDoubleBinding(() -> pitchStack.getWidth() / pitchView.getImage().getWidth(),
                pitchStack.widthProperty());
        pitchScaleX = new SimpleDoubleProperty(1);
        pitchScaleX.bind(scaleXBinding);

        DoubleBinding scaleYBindings = Bindings.createDoubleBinding(() -> pitchStack.getHeight() / pitchView.getImage().getHeight(),
                pitchStack.heightProperty());
        pitchScaleY = new SimpleDoubleProperty(1);
        pitchScaleY.bind(scaleYBindings);
    }

    private EventHandler<MouseEvent> createDragDetectedHandler(
            final TableCell cell) {
        return event -> {
            if (cell.getIndex() < cell.getTableView().getItems().size()) {
                Dragboard db = cell.startDragAndDrop(TransferMode.MOVE);
                ClipboardContent content = new ClipboardContent();
                content.putString(subPlayersTable.getItems().get(cell.getIndex()).getId());
                db.setContent(content);

                Image image = new Image(PitchController.class.getResourceAsStream("/images/sub.png"), 22, 22, true, true);
                db.setDragView(image);
            }
        };
    }

    private void initalizeDragAndDrop() {
        pitchPanel.setOnMouseDragEntered(event -> initializeDraggingNode(event));

        pitchPanel.setOnMouseDragOver(event -> {
            if (event.getButton() == MouseButton.PRIMARY || event.getButton() == MouseButton.SECONDARY) {
                relocateDraggingNode(event.getSceneX(), event.getSceneY());
            }
        });
    }

    private void initializeDraggingNode(MouseDragEvent event) {
        currentDraggingNewPlayer = (PlayerGraphic) event.getGestureSource();

        if (event.getButton() == MouseButton.PRIMARY) {
            draggingNode = draggingBall;
            currentDraggingNewPlayer.draggingProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) {
                    DirectMediaPlayer mediaPlayer = ResizableMediaPlayer.getInstance().getMediaPlayer();
                    if (playerWasRunning && !mediaPlayer.isPlaying() && !footballEventContextMenu.isShowing()) {
                        mediaPlayer.play();
                        playerWasRunning = false;
                    }
                }
            });

        } else if (event.getButton() == MouseButton.SECONDARY) {
            draggingNode = draggingPlayerGraphic;

            draggingPlayerGraphic.setText(currentDraggingNewPlayer.getText());
            currentDraggingNewPlayer.draggingProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) {
                    positionGraphics.forEach(positionGraphic -> positionGraphic.setVisible(false));
                }
            });
            positionGraphics.forEach(positionGraphic -> positionGraphic.setVisible(true));
        }

        if (event.getButton() == MouseButton.PRIMARY || event.getButton() == MouseButton.SECONDARY) {
            if (!(pitchPanel.getChildren().contains(draggingNode))) {
                pitchPanel.getChildren().add(draggingNode);
            }

            currentDraggingNewPlayer.draggingProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) {
                    pitchPanel.getChildren().remove(draggingNode);
                    currentDraggingNewPlayer = null;
                }
            });

            draggingNode.relocate(0, 0);
            startRelocateDraggingX = -draggingNode.getBoundsInParent().getMinX();
            startRelocateDraggingY = -draggingNode.getBoundsInParent().getMinY();
            draggingNode.relocate(startRelocateDraggingX, startRelocateDraggingY);
        }
    }

    @FXML
    private void showEdit() {
        Dialog dialog = new Dialog();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(Main.class.getResource("/view/edit/PlayersMatchHistory.fxml"));
        try {
            DialogPane dialogPane = fxmlLoader.load();
            PlayersMatchHistoryController controller = fxmlLoader.getController();
            controller.setTeam(currentMatchHistory.get().getSourceTeam());
            controller.setSubPlayers(subPlayersTable.getItems());
//            controller.setStage(stage);

            dialog.setDialogPane(dialogPane);
            dialog.setTitle("Edytuj zawodników historii meczu");

            Button button = (Button) dialog.getDialogPane().lookupButton(ButtonType.CLOSE);
            button.setText("Zamknij");
            dialog.initStyle(StageStyle.DECORATED);
            dialog.initModality(Modality.WINDOW_MODAL);
            dialog.setResizable(true);
            Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
            stage.getIcons().add(ResourceFileManager.getImage("icon.png"));
//            dialog.initOwner();
            dialog.showAndWait();

        } catch (IOException e) {
//            showError("Błąd podczas otwierania okna \"O programie\"", "", "Wystąpił nieoczekiwany błąd podczas otwierania okna");
//            logger.error("Błąd podczas otwierania okna \"O programie\"", e);
            e.printStackTrace();
        }
    }

    private void relocateDraggingNode(double sceneX, double sceneY) {
        Point2D localPoint = pitchPanel.sceneToLocal(new Point2D(sceneX, sceneY));
        Point2D newPoint = new Point2D((int) (localPoint.getX() - draggingNode.getBoundsInParent().getWidth() / 2),
                (localPoint.getY() - draggingNode.getBoundsInParent().getHeight() / 2));
        draggingNode.relocate(startRelocateDraggingX + newPoint.getX(), startRelocateDraggingY + newPoint.getY());
    }

    public void setFootballEventsList(ObservableList<FootballEvent> footballEventsList) {
        eventsTable.setItems(footballEventsList);
    }

    public void setSubPlayers(ObservableList<Player> subPlayers) {
        subPlayersTable.setItems(subPlayers);
    }

    public ObjectProperty<MatchHistory> currentMatchHistoryProperty() {
        return currentMatchHistory;
    }

    public ObservableList<PlayerGraphic> getPlayerGraphics() {
        return playerGraphics;
    }

    public void setPlayerGraphics(List<PlayerGraphic> playerGraphics) {
        this.playerGraphics.setAll(playerGraphics);
        initPausingWhenAddindEvent(this.playerGraphics);
    }

    public void setFirstSquadOnPitch(ObservableList<Player> firstSquadOnPitch) {
        playerGraphics.clear();

        //GK
        Player player;
        PlayerGraphic playerGraphic;
        PositionGraphic positionGraphic;
        positionGraphic = positionGraphics.get(0);
        player = firstSquadOnPitch.get(0);
        playerGraphic = new PlayerGraphic(player, positionGraphic);
        playerGraphics.add(playerGraphic);

        for (int i = 1, j = 1, k = 0; i < firstSquadOnPitch.size(); i++) {
            player = firstSquadOnPitch.get(i);
            positionGraphic = positionGraphics.get(j + (5 * k));
            playerGraphic = new PlayerGraphic(player, positionGraphic);
            playerGraphics.add(playerGraphic);

            if (++j == 3) {
                ++j;
            }
            if (j == 6) {
                j = 1;
                k += 2;
                if (k == 4) {
                    ++j;
                }
            }
        }

        initPausingWhenAddindEvent(playerGraphics);
    }

    private void initPausingWhenAddindEvent(ObservableList<PlayerGraphic> playerGraphics) {
        final DirectMediaPlayer mediaPlayer = ResizableMediaPlayer.getInstance().getMediaPlayer();

        playerGraphics.forEach(playerGraphic -> playerGraphic.setOnMousePressed(event -> {
            if (event.getButton() == MouseButton.PRIMARY) {
                playerWasRunning = mediaPlayer.isPlaying();

                if (playerWasRunning) {
                    mediaPlayer.pause();
                }
            }
        }));

        footballEventContextMenu.setOnHidden(event -> {
            if (playerWasRunning && !mediaPlayer.isPlaying()) {
                mediaPlayer.play();
                playerWasRunning = false;
            }
        });
    }

    public List<PositionGraphic> getPositionGraphicList() {
        return positionGraphics;
    }

    public static Optional<PositionGraphic> getPositionGraphicById(String textPositionGraphic) {
        return positionGraphics.stream()
                .filter(positionGraphic ->
                        positionGraphic.getText().equalsIgnoreCase(textPositionGraphic))
                .findFirst();
    }

    public void setCurrentMatchHalf(FootballEvent.Halfs currentMatchHalf) {
        this.currentMatchHalf = currentMatchHalf;
    }
}

