package pl.mmaciega.matchanalyzer.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;
import pl.mmaciega.matchanalyzer.controller.DatabaseController;
import pl.mmaciega.matchanalyzer.util.ResourceFileManager;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class Country {
    private StringProperty namePl;
    private StringProperty nameEn;
    private StringProperty code;
    private Image flag;

    public Country() {
        this(null,null,null);
    }

    public Country(String namePl, String nameEn, String code) {
        this.namePl = new SimpleStringProperty(namePl);
        this.nameEn = new SimpleStringProperty(nameEn);
        this.code = new SimpleStringProperty(code);
    }

    @XmlTransient
    public Image getImageFlag() {
        if (flag == null) {
            flag = ResourceFileManager.getImage(DatabaseController.COUNTRIES_FLAGS_DIRECTORY + getCode() + ".png");
        }

        return flag;
    }

    public StringProperty namePlProperty() {
        return namePl;
    }
    @XmlElement(required = true)
    public String getNamePl() {
        return namePl.get();
    }
    private void setNamePl(String namePl) {
        this.namePl.set(namePl);
    }

    public StringProperty nameEnProperty() {
        return nameEn;
    }
    @XmlElement(required = true)
    public String getNameEn() {
        return nameEn.get();
    }
    private void setNameEn(String nameEn) {
        this.nameEn.set(nameEn);
    }

    public StringProperty codeProperty() {
        return code;
    }
    @XmlElement(required = true)
    public String getCode() {
        return code.get();
    }
    private void setCode(String code) {
        this.code.set(code.toLowerCase());
    }

    @Override
    public String toString() {
        return getNamePl();
    }

}
