package pl.mmaciega.matchanalyzer.model;

import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import pl.mmaciega.matchanalyzer.util.jaxb.adapters.CountryXmlAdapter;
import pl.mmaciega.matchanalyzer.util.jaxb.adapters.PositionXmlAdapter;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;

public class Player {
    private StringProperty id;
    private StringProperty name;
    private IntegerProperty number;
    private ObjectProperty<Country> country;
    private ObjectProperty<Position> position;
    private ObjectProperty<Team> team;

    public Player() {
        this(null, -1, null, null, null);
    }

    public Player(String name, int number, Country country, Position position, Team team) {
        this.name = new SimpleStringProperty(name);
        this.number = new SimpleIntegerProperty(number);
        this.country = new SimpleObjectProperty<>(country);
        this.position = new SimpleObjectProperty<>(position);
        this.team = new SimpleObjectProperty<>(team);
        this.id = new SimpleStringProperty();
    }


    public void afterUnmarshal(Unmarshaller u, Object parent) {
        this.team.set((Team) parent);
        this.id.bind(Bindings.createStringBinding(() -> {
            if (getTeam() == null) {
                return null;
            } else {
                return getNumber() + getTeam().getName().toUpperCase();
            }
        }, this.number, this.team));
    }

    @XmlAttribute(required = true)
    @XmlID
    public String getId() {
        return id.get();
    }
    private void setId(String id) {
        this.id.set(id);
    }

    public StringProperty nameProperty() {
        return name;
    }
    @XmlElement(required = true)
    public String getName() {
        return name.get();
    }
    public void setName(String name) {
        this.name.set(name);
    }

    public IntegerProperty numberProperty() {
        return number;
    }
    @XmlElement(required = true)
    public int getNumber() {
        return number.get();
    }
    public void setNumber(int number) {
        this.number.set(number);
    }

    public ObjectProperty<Country> countryProperty() {
        return country;
    }
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CountryXmlAdapter.class)
    public Country getCountry() {
        return country.get();
    }
    public void setCountry(Country country) {
        this.country.set(country);
    }

    public ObjectProperty<Position> positionProperty() {
        return position;
    }
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(PositionXmlAdapter.class)
    public Position getPosition() {
        return position.get();
    }
    public void setPosition(Position position) {
        this.position.set(position);
    }

    public ObjectProperty<Team> teamProperty() {
        return team;
    }
    @XmlTransient
    public Team getTeam() {
        return team.get();
    }
    public void setTeam(Team team) {
        this.team.set(team);
        this.id.bind(Bindings.createStringBinding(() -> {
            if (getTeam() == null) {
                return null;
            } else {
                return getNumber() + getTeam().getName().toUpperCase();
            }
        }, this.number, this.team));
    }

    public Player deepClone() {
        Player clonePlayer = new Player();
        clonePlayer.setId(getId());
        clonePlayer.setName(getName());
        clonePlayer.setNumber(getNumber());
        clonePlayer.setCountry(getCountry());
        clonePlayer.setPosition(getPosition());
        return clonePlayer;
    }
}
