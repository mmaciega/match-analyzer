package pl.mmaciega.matchanalyzer.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class Position {
    private IntegerProperty id;
    private StringProperty namePl;
    private StringProperty codePl;
    private StringProperty nameEn;
    private StringProperty codeEn;

    public Position() {
        this(-1, null,null,null, null);
    }

    public Position(int id, String namePl, String codePl, String nameEn, String codeEn) {
        this.id = new SimpleIntegerProperty(id);
        this.namePl = new SimpleStringProperty(namePl);
        this.codePl = new SimpleStringProperty(codePl);
        this.nameEn = new SimpleStringProperty(nameEn);
        this.codeEn = new SimpleStringProperty(codeEn);
    }

    public IntegerProperty idProperty() {
        return id;
    }
    @XmlElement(required = true)
    public int getId() {
        return id.get();
    }
    public void setId(int id) {
        this.id.set(id);
    }

    public StringProperty namePlProperty() {
        return namePl;
    }
    @XmlElement(required = true)
    public String getNamePl() {
        return namePl.get();
    }
    private void setNamePl(String namePl) {
        this.namePl.set(namePl);
    }

    public StringProperty codePlProperty() {
        return codePl;
    }
    @XmlElement(required = true)
    public String getCodePl() {
        return codePl.get();
    }
    private void setCodePl(String codePl) {
        this.codePl.set(codePl);
    }

    public StringProperty nameEnProperty() {
        return nameEn;
    }
    @XmlElement(required = true)
    public String getNameEn() {
        return nameEn.get();
    }
    private void setNameEn(String nameEn) {
        this.nameEn.set(nameEn);
    }

    public StringProperty codeEnProperty() {
        return codeEn;
    }
    @XmlElement(required = true)
    public String getCodeEn() {
        return codeEn.get();
    }
    private void setCodeEn(String codeEn) {
        this.codeEn.set(codeEn);
    }

    @Override
    public String toString() {
        return getNamePl();
    }
}
