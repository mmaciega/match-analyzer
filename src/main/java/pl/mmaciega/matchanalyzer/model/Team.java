package pl.mmaciega.matchanalyzer.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import pl.mmaciega.matchanalyzer.util.jaxb.adapters.CountryXmlAdapter;
import pl.mmaciega.matchanalyzer.util.jaxb.adapters.ImageXmlAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.List;

public class Team {
    private StringProperty name;
    private ObjectProperty<Country> country;

    private ObjectProperty<Image> image = new SimpleObjectProperty<>();
    private StringProperty logoPath = new SimpleStringProperty();
    private ObservableList<Player> players = FXCollections.observableArrayList();

    public Team() {
        this(null,null);
    }

    public Team(String name, Country country) {
        this.name = new SimpleStringProperty(name);
        this.country = new SimpleObjectProperty<>(country);
    }

    public StringProperty nameProperty() {
        return name;
    }
    @XmlID
    @XmlElement(required = true)
    public String getName() {
        return name.get();
    }
    public void setName(String name) {
        this.name.set(name);
    }

    public ObjectProperty<Country> countryProperty() {
        return country;
    }
    @XmlJavaTypeAdapter(CountryXmlAdapter.class)
    @XmlElement(required = true)
    public Country getCountry() {
        return country.get();
    }
    public void setCountry(Country country) {
        this.country.set(country);
    }

    public StringProperty logoPathProperty() {
        return logoPath;
    }
    @XmlTransient
    public String getLogoPath() {
        return logoPath.get();
    }
    public void setLogoPath(String logoPath) {
        this.logoPath.set(logoPath);
    }

    public ObjectProperty<Image> imageProperty() {
        return image;
    }
    @XmlJavaTypeAdapter(ImageXmlAdapter.class)
    public Image getImage() {
        return image.get();
    }
    public void setImage(Image image) {
        this.image.set(image);
    }

    public ObservableList<Player> getPlayers() {
        return players;
    }

    @XmlElementWrapper(name = "players")
    @XmlElement(name = "player")
    private List<Player> getPlayersWrapper() {
        return players;
    }

    @Override
    public String toString() {
        return getName();
    }

    public Team deepClone() {
        Team cloneTeam = new Team();
        cloneTeam.setName(getName());
        cloneTeam.setCountry(getCountry());
        cloneTeam.setLogoPath(getLogoPath());

        if (getImage() != null) {
            BufferedImage bufferedImage = SwingFXUtils.fromFXImage(getImage(), null);
            Image cloneImage = SwingFXUtils.toFXImage(bufferedImage, null);
            cloneTeam.setImage(cloneImage);
        } else {
            cloneTeam.setImage(null);
        }

        for(Player player : getPlayers()) {
            Player clonePlayer = player.deepClone();
            clonePlayer.setTeam(cloneTeam);
            cloneTeam.getPlayers().add(clonePlayer);
        }


        return cloneTeam;
    }
}
