package pl.mmaciega.matchanalyzer.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import pl.mmaciega.matchanalyzer.model.events.DirectEvent;
import pl.mmaciega.matchanalyzer.model.events.FootballEvent;
import pl.mmaciega.matchanalyzer.util.jaxb.adapters.LocalDateTimeXmlAdapter;
import pl.mmaciega.matchanalyzer.util.jaxb.adapters.LocalDateXmlAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class MatchHistory {
    private ObjectProperty<Team> hostTeam;
    private ObjectProperty<Team> guestTeam;
    private ObjectProperty<Team> sourceTeam;
    private ObjectProperty<LocalDate> matchDate;
    private ObjectProperty<LocalDateTime> createDateTime;

    private ObservableList<Player> firstSquad = FXCollections.observableArrayList();
    private ObservableList<FootballEvent> footballEvents = FXCollections.observableArrayList();

    public MatchHistory() {
        this(null, null, null, null, null);
    }

    public MatchHistory(Team hostTeam, Team guestTeam, Team sourceTeam, ObservableList<Player> firstSquad, LocalDate matchDate) {
        this.hostTeam = new SimpleObjectProperty<>(hostTeam);
        this.guestTeam = new SimpleObjectProperty<>(guestTeam);
        this.sourceTeam = new SimpleObjectProperty<>(sourceTeam);
        this.firstSquad = firstSquad;
        this.matchDate = new SimpleObjectProperty<>(matchDate);
        this.createDateTime = new SimpleObjectProperty<>(LocalDateTime.now());
    }

    public ObjectProperty<Team> hostTeamProperty() {
        return hostTeam;
    }
    @XmlElement(required = true)
    public Team getHostTeam() {
        return hostTeam.get();
    }
    public void setHostTeam(Team hostTeam) {
        this.hostTeam.set(hostTeam);
    }

    public ObjectProperty<Team> guestTeamProperty() {
        return guestTeam;
    }
    @XmlElement(required = true)
    public Team getGuestTeam() {
        return guestTeam.get();
    }
    public void setGuestTeam(Team guestTeam) {
        this.guestTeam.set(guestTeam);
    }

    public ObjectProperty<Team> sourceTeamProperty() {
        return sourceTeam;
    }
    @XmlIDREF
    @XmlElement(name = "sourceTeamName", required = true)
    public Team getSourceTeam() {
        return sourceTeam.get();
    }
    public void setSourceTeam(Team sourceTeam) {
        this.sourceTeam.set(sourceTeam);
    }

    public ObjectProperty<LocalDate> matchDateProperty() {
        return matchDate;
    }
    @XmlJavaTypeAdapter(LocalDateXmlAdapter.class)
    public LocalDate getMatchDate() {
        return matchDate.get();
    }
    public void setMatchDate(LocalDate matchDate) {
        this.matchDate.set(matchDate);
    }

    public ObjectProperty<LocalDateTime> createDateTimeProperty() {
        return createDateTime;
    }
    @XmlJavaTypeAdapter(LocalDateTimeXmlAdapter.class)
    @XmlElement(required = true)
    public LocalDateTime getCreateDateTime() {
        return createDateTime.get();
    }
    public void setCreateDateTime(LocalDateTime createDateTime) {

        this.createDateTime.set(createDateTime);
    }

    @XmlTransient
    public ObservableList<Player> getFirstSquad() {
        return firstSquad;
    }
    public void setFirstSquad(ObservableList<Player> firstSquad) {
        this.firstSquad = firstSquad;
    }

    public ObservableList<FootballEvent> getFootballEvents() {
        return footballEvents;
    }

    public FilteredList<Player> getSubPlayers() {
        return getSourceTeam().getPlayers().filtered(player -> !firstSquad.contains(player));
    }

    @XmlElementWrapper(name = "events")
    @XmlElements({
            @XmlElement(name = "selfEvent", type = FootballEvent.class),
            @XmlElement(name = "targetEvent", type = DirectEvent.class)})
    private List<FootballEvent> getFootballEventsWrapper() {
        return footballEvents;
    }

}
