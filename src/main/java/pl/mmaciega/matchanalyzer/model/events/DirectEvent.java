package pl.mmaciega.matchanalyzer.model.events;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import pl.mmaciega.matchanalyzer.model.Player;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;

public class DirectEvent extends FootballEvent {
    private ObjectProperty<Player> destinationPlayer;

    public DirectEvent() {
        this(null, null, null, null, null, null);
    }

    public DirectEvent(Player sourcePlayer, Player destinationPlayer, Halfs half, Regions region, Types type, Boolean successful) {
        super(sourcePlayer, half, region, type, successful);

        this.destinationPlayer = new SimpleObjectProperty<>(destinationPlayer);
    }

    public ObjectProperty<Player> destinationPlayerProperty() {
        return destinationPlayer;
    }
    @XmlElement(name = "destinationPlayerRef", required = true)
    @XmlIDREF
    public Player getDestinationPlayer() {
        return destinationPlayer.get();
    }
    public void setDestinationPlayer(Player destinationPlayer) {
        this.destinationPlayer.set(destinationPlayer);
    }
}


