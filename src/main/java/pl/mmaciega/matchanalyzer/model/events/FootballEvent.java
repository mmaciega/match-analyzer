package pl.mmaciega.matchanalyzer.model.events;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import pl.mmaciega.matchanalyzer.model.Player;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import java.util.EnumSet;
import java.util.Set;

public class FootballEvent {
    public enum Types {
        CROSS("dośrodkowanie"), DRIBBLE("drybling"), KEY_PASS("kluczowe podanie"),
        TACKLING("odbiór"), PASS("podanie"), INTERCEPTION("przechwycenie"),
        LOST("strata"),  SHOOT("strzał");

        private final String name;

        Types(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public static final Set<Types> SELF_TYPES = EnumSet.of(Types.LOST, Types.TACKLING, Types.DRIBBLE, Types.INTERCEPTION, Types.SHOOT, Types.CROSS);
    public static final Set<Types> TARGET_TYPES = EnumSet.of(Types.PASS, Types.KEY_PASS);

    public enum Regions {
        LG, SG, PG,
        LS, S, PS,
        LD, SD, PD
    }

    public enum Halfs {
        FIRST("1"), SECOND("2");

        private Halfs(String value) {
            this.value = value;
        }

        private final String value;

        public String getValue(){ return value; }

        public static Halfs fromString(String text) {
            if (text != null) {
                for (Halfs b : Halfs.values()) {
                    if (text.equalsIgnoreCase(b.value)) {
                        return b;
                    }
                }
            }
            return null;
        }
    }

    private ObjectProperty<Player> sourcePlayer;
    private ObjectProperty<Halfs> half;
    private BooleanProperty successful;
    private ObjectProperty<Regions> region;
    private ObjectProperty<Types> type;

    public FootballEvent() {
        this(null, null, null, null, null);
    }

    public FootballEvent(Player sourcePlayer, Halfs half, Regions region, Types type, Boolean successful) {
        this.sourcePlayer = new SimpleObjectProperty<>(sourcePlayer);
        this.half = new SimpleObjectProperty<>(half);
        this.region = new SimpleObjectProperty<>(region);
        this.type = new SimpleObjectProperty<>(type);
        this.successful = new SimpleBooleanProperty(successful == null ? false : successful);
    }

    public ObjectProperty<Player> sourcePlayerProperty() {
        return sourcePlayer;
    }
    @XmlElement(name = "sourcePlayerRef", required = true)
    @XmlIDREF
    public Player getSourcePlayer() {
        return sourcePlayer.get();
    }
    public void setSourcePlayer(Player sourcePlayer) {
        this.sourcePlayer.set(sourcePlayer);
    }

    public ObjectProperty<Halfs> halfProperty() {
        return half;
    }
    @XmlElement(required = true)
    public Halfs getHalf() {
        return half.get();
    }
    public void setHalf(Halfs half) {
        this.half.set(half);
    }

    public BooleanProperty successfulProperty() {
        return successful;
    }
    @XmlElement(required = true)
    public boolean getSuccessful() {
        return successful.get();
    }
    public void setSuccessful(boolean successful) {
        this.successful.set(successful);
    }

    public ObjectProperty<Types> typeProperty() {
        return type;
    }
    @XmlElement(required = true)
    public Types getType() {
        return type.get();
    }
    public void setType(Types type) {
        this.type.set(type);
    }

    public ObjectProperty<Regions> regionProperty() {
        return region;
    }
    @XmlElement(required = true)
    public Regions getRegion() {
        return region.get();
    }
    public void setRegion(Regions region) {
        this.region.set(region);
    }
}
