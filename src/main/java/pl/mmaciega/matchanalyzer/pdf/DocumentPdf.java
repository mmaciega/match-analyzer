package pl.mmaciega.matchanalyzer.pdf;

import com.itextpdf.awt.DefaultFontMapper;
import com.itextpdf.awt.PdfPrinterGraphics2D;
import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.*;
import javafx.embed.swing.SwingFXUtils;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.general.DefaultPieDataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.mmaciega.matchanalyzer.Main;
import pl.mmaciega.matchanalyzer.model.MatchHistory;
import pl.mmaciega.matchanalyzer.model.events.FootballEvent;
import pl.mmaciega.matchanalyzer.stats.MatchHistoryStats;
import pl.mmaciega.matchanalyzer.stats.TypeStats;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DocumentPdf {
    private final Logger logger = LoggerFactory.getLogger(DocumentPdf.class);

    private MatchHistory matchHistory;
    private MatchHistoryStats matchHistoryStats;
    private String filePath;
    private Document document;

    private BaseFont baseFont;

    public DocumentPdf(MatchHistory matchHistory, String filePath) throws DocumentException, IOException {
        this.matchHistory = matchHistory;
        this.filePath = filePath;

        matchHistoryStats = new MatchHistoryStats(matchHistory);
        document = new Document(PageSize.A4);

        try {
            baseFont = BaseFont.createFont("c:/windows/fonts/verdana.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        } catch (DocumentException e) {
            logger.error("Czcionka wykorzystywana w dokumencie jest nieprawidłowa", e);
            throw e;
        } catch (IOException e) {
            logger.error("Nie została znaleziona czcionka pod podanym adresem", e);
            throw e;
        }

    }

    public void generate() throws IOException, DocumentException {
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filePath));
        writer.setStrictImageSequence(true);
        writer.setPageEvent(new PdfEventHandler());

        document.open();
        generateFirstPage();

        Chapter chapter = new Chapter(1);
        generateTeamStatsPage(chapter, writer);
        generateIndividualStatsPages(chapter);
        document.add(chapter);
        chapter.flushContent();

        generatePitchPages();
        document.close();
    }

    private void generateFirstPage() throws DocumentException {
        //Title
        Paragraph p = new Paragraph("Raport meczowy", new Font(baseFont, 45));
        p.setAlignment(Element.ALIGN_CENTER);
        p.setSpacingAfter(50);
        document.add(p);

        //Logos
        p = new Paragraph();
        p.setAlignment(Element.ALIGN_CENTER);
        javafx.scene.image.Image imageFx = matchHistory.getSourceTeam().getImage();
        addLogoToParagraph(p, imageFx, 200);
        document.add(p);

        //Teams names
        p = new Paragraph(matchHistory.getHostTeam().getName() + " - " + matchHistory.getGuestTeam().getName(), new Font(baseFont, 25));
        p.setAlignment(Element.ALIGN_CENTER);
        p.setSpacingBefore(20);
        document.add(p);

        //Match date
        if (matchHistory.getMatchDate() != null) {
            p = new Paragraph(matchHistory.getMatchDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")), new Font(baseFont, 20));
            p.setAlignment(Element.ALIGN_CENTER);
            p.setSpacingBefore(20);
            document.add(p);
        }
    }

    private void generatePitchPages() throws DocumentException, IOException {
        Paragraph p = new Paragraph();
        p.setAlignment(Element.ALIGN_CENTER);
        p.add(new Chunk("Obecność w strefach", new Font(baseFont, 35)));
        p.add(Chunk.NEWLINE);
        p.add(Chunk.NEWLINE);
        p.add(new Chunk("Ujęcie graficzne", new Font(baseFont, 22)));
        p.setSpacingAfter(55);
        Chapter chapter = new Chapter(p, 2);
        chapter.setNumberDepth(0);

        document.newPage();
        addPitchTable(chapter, "Pierwsza połowa", FootballEvent.Halfs.FIRST);

        chapter.newPage();
        addPitchTable(chapter, "Druga połowa", FootballEvent.Halfs.SECOND);

        chapter.newPage();
        addPitchTable(chapter, "Cały mecz", null);

        document.add(chapter);
        chapter.flushContent();
    }

    private void addPitchTable(Chapter chapter, String headerText, FootballEvent.Halfs half) throws BadElementException {
        Paragraph p;
        p = new Paragraph();
        p.setAlignment(Element.ALIGN_CENTER);
        p.add(new Chunk(headerText, new Font(baseFont, 22)));
        p.setSpacingAfter(15);
        Section section = chapter.addSection(p);
        section.setNumberDepth(0);

        PdfPTable table = new PdfPTable(3);
        table.setWidthPercentage(100f);
        table.setTableEvent(new TableEvent());
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.getDefaultCell().setFixedHeight(124.6f);
        fillPitchTable(table, half);

        section.add(Chunk.NEWLINE);
        section.add(table);
        section.add(Chunk.NEWLINE);
        section.add(Chunk.NEWLINE);

        p = new Paragraph();
        p.setAlignment(Element.ALIGN_CENTER);
        javafx.scene.image.Image imageFx = matchHistory.getSourceTeam().getImage();
        addLogoToParagraph(p, imageFx, 150);
        section.add(p);
    }

    private void fillPitchTable(PdfPTable table, FootballEvent.Halfs half) {
        Font font = new Font(Font.FontFamily.HELVETICA, 18);

        Map<FootballEvent.Regions, List<FootballEvent>> regionMap;
        long numberAllEvents;

        if (half != null) {
            regionMap = matchHistory.getFootballEvents()
                    .stream()
                    .filter(footballEvent -> footballEvent.getHalf() == half)
                    .collect(Collectors.groupingBy(FootballEvent::getRegion));
            numberAllEvents = matchHistory.getFootballEvents().stream().filter(footballEvent -> footballEvent.getHalf() == half).count();
        } else {
            regionMap = matchHistory.getFootballEvents()
                    .stream()
                    .collect(Collectors.groupingBy(FootballEvent::getRegion));
            numberAllEvents = matchHistory.getFootballEvents().size();
        }

        LinkedHashMap<FootballEvent.Regions, Integer> sortedRegionMap = regionMap.entrySet()
                .stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().size(), (e1, e2) -> e1, LinkedHashMap::new));

        NumberFormat defaultFormat = NumberFormat.getPercentInstance();
        defaultFormat.setMinimumFractionDigits(1);
        Integer size = sortedRegionMap.get(FootballEvent.Regions.LG);
        Phrase phrase = new Phrase(size != null ? String.valueOf(defaultFormat.format((double) size / numberAllEvents)) : "0%", font);
        table.addCell(phrase);
        size = sortedRegionMap.get(FootballEvent.Regions.SG);
        phrase = new Phrase(size != null ? String.valueOf(defaultFormat.format((double) size / numberAllEvents)) : "0%", font);
        table.addCell(phrase);
        size = sortedRegionMap.get(FootballEvent.Regions.PG);
        phrase = new Phrase(size != null ? String.valueOf(defaultFormat.format((double) size / numberAllEvents)) : "0%", font);
        table.addCell(phrase);
        size = sortedRegionMap.get(FootballEvent.Regions.LS);
        phrase = new Phrase(size != null ? String.valueOf(defaultFormat.format((double) size / numberAllEvents)) : "0%", font);
        table.addCell(phrase);
        size = sortedRegionMap.get(FootballEvent.Regions.S);
        phrase = new Phrase(size != null ? String.valueOf(defaultFormat.format((double) size / numberAllEvents)) : "0%", font);
        table.addCell(phrase);
        size = sortedRegionMap.get(FootballEvent.Regions.PS);
        phrase = new Phrase(size != null ? String.valueOf(defaultFormat.format((double) size / numberAllEvents)) : "0%", font);
        table.addCell(phrase);
        size = sortedRegionMap.get(FootballEvent.Regions.LD);
        phrase = new Phrase(size != null ? String.valueOf(defaultFormat.format((double) size / numberAllEvents)) : "0%", font);
        table.addCell(phrase);
        size = sortedRegionMap.get(FootballEvent.Regions.SD);
        phrase = new Phrase(size != null ? String.valueOf(defaultFormat.format((double) size / numberAllEvents)) : "0%", font);
        table.addCell(phrase);
        size = sortedRegionMap.get(FootballEvent.Regions.PD);
        phrase = new Phrase(size != null ? String.valueOf(defaultFormat.format((double) size / numberAllEvents)) : "0%", font);
        table.addCell(phrase);
    }

    private void generateIndividualStatsPages(Chapter chapter) {
        chapter.newPage();
        Paragraph p = new Paragraph();
        p.setAlignment(Element.ALIGN_CENTER);
        p.add(new Chunk("STATYSTYKI ZAWODNIKÓW", new Font(baseFont, 22)));
        p.setSpacingAfter(25);
        Section section2 = chapter.addSection(p);
        section2.setNumberDepth(0);

        Map<FootballEvent.Types, List<FootballEvent>> collectByType = matchHistory.getFootballEvents().stream()
                .collect(Collectors.groupingBy(FootballEvent::getType));
        for (Map.Entry<FootballEvent.Types, List<FootballEvent>> element : collectByType.entrySet()) {
            p = new Paragraph();
            p.setAlignment(Element.ALIGN_LEFT);
            Chunk chunk = new Chunk(getProperText(element.getKey()), new Font(baseFont, 15));
            p.add(chunk);
            Section section21 = section2.addSection(p);

            try {
                SheetTable sheetTable = new SheetTable(new TypeStats(element.getKey()), matchHistory.getFootballEvents());
                sheetTable.setHeaderData(getProperText(element.getKey()));
                sheetTable.initialize();
                section21.add(sheetTable);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }

            section2.newPage();
        }

    }

    private String getProperText(FootballEvent.Types type) {
        String result = null;
        switch (type) {
            case KEY_PASS:
                result = "Liczba kluczowych podań";
                break;
            case PASS:
                result = "Liczba podań";
                break;
            case DRIBBLE:
                result = "Liczba dryblingów";
                break;
            case CROSS:
                result = "Liczba dośrodkowań";
                break;
            case INTERCEPTION:
                result = "Liczba przechwytów";
                break;
            case LOST:
                result = "Liczba strat";
                break;
            case SHOOT:
                result = "Liczba strzałów";
                break;
            case TACKLING:
                result = "Liczba odbiorów";
                break;
        }

        return result;
    }

    private void generateTeamStatsPage(Chapter chapter, PdfWriter writer) throws DocumentException {
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.add(new Chunk("Współdziałanie", new Font(baseFont, 35)));
        paragraph.add(Chunk.NEWLINE);
        paragraph.add(Chunk.NEWLINE);
        paragraph.add(new Chunk("Ujęcie liczbowe", new Font(baseFont, 22)));
        paragraph.setSpacingAfter(55);
        chapter.setTitle(paragraph);
        chapter.setNumberDepth(0);

        document.newPage();

        paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.add(new Chunk("STATYSTYKI ZESPOŁOWE", new Font(baseFont, 22)));
        paragraph.setSpacingAfter(15);
        Section section1 = chapter.addSection(paragraph);
        section1.setNumberDepth(0);

        Map<String, String> teamStats = matchHistoryStats.generateTeamStats();
        DefaultPieDataset dataset = new DefaultPieDataset();
        for (Map.Entry<String, String> element : teamStats.entrySet()) {
            dataset.setValue(element.getKey(), Double.parseDouble(element.getValue()));
        }

        document.add(chapter);
        chapter.flushContent();

        createPieChart(writer, dataset);
    }

    private void createPieChart(PdfWriter writer, DefaultPieDataset dataset) {
        DefaultFontMapper mapper = new DefaultFontMapper();
        mapper.insertDirectory("c:/windows/fonts/");
        DefaultFontMapper.BaseFontParameters parameters = new DefaultFontMapper.BaseFontParameters("c:/windows/fonts/verdana.ttf");
        parameters.encoding = BaseFont.IDENTITY_H;
        mapper.putName("Verdana", parameters);
        PdfContentByte contentByte = writer.getDirectContent();
        PdfTemplate template = contentByte.createTemplate(document.getPageSize().getWidth(), writer.getPageSize().getHeight());
        Graphics2D graphics2d = new PdfPrinterGraphics2D(template, 500, 400, mapper, null);
        Rectangle2D rectangle2d = new Rectangle2D.Double(0, 0, 500, 400);

        JFreeChart chart = ChartFactory.createPieChart("", dataset, true, true, false);
        TextTitle t = chart.getTitle();
        t.setFont(new java.awt.Font("Verdana", Font.NORMAL, 18));
        chart.removeLegend();

        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setNoDataMessage("Dane niedostępne");
        plot.setLabelGap(0.02);
        plot.setLabelFont(new java.awt.Font("Verdana", Font.NORMAL, 10));
        PieSectionLabelGenerator gen = new StandardPieSectionLabelGenerator(
                "{0}: {1} ({2})", new DecimalFormat("0"), new DecimalFormat("0%"));
        plot.setLabelGenerator(gen);
        plot.setSimpleLabels(true);
        plot.setBackgroundPaint(Color.WHITE);

        chart.draw(graphics2d, rectangle2d);

        graphics2d.dispose();
        contentByte.addTemplate(template, (document.getPageSize().getWidth() / 2) - ((float) rectangle2d.getWidth() / 2), 220);
    }

    private void addLogoToParagraph(Paragraph p, javafx.scene.image.Image imageFx, float fitSize) throws BadElementException {
        if (imageFx != null) {
            BufferedImage bImage = SwingFXUtils.fromFXImage(imageFx, null);
            ByteArrayOutputStream s = new ByteArrayOutputStream();
            try {
                ImageIO.write(bImage, "png", s);
                Image image = Image.getInstance(s.toByteArray());
                image.scaleToFit(fitSize, fitSize);
                image.setAlignment(Image.ALIGN_CENTER);

                p.add(new Chunk(image, 0, 0, true));
            } catch (IOException e) {
                logger.error("Wystąpił problem podczas zapisu do tablicy bajtów logo drużyny", e);
            }
        }
    }

    private class TableEvent implements PdfPTableEvent {
        @Override
        public void tableLayout(PdfPTable table, float[][] widths, float[] heights, int headerRows, int rowStart, PdfContentByte[] canvases) {
            try {
                Image img = Image.getInstance(Main.class.getResource("/images/boiska cale duze.jpg"));
                img.scaleToFit(table.getTotalWidth(), table.getTotalHeight());
                System.out.println(table.getTotalWidth() + " " + table.getTotalHeight());
                img.setAbsolutePosition(widths[0][0], heights[0] - img.getScaledHeight());
                canvases[PdfPTable.BACKGROUNDCANVAS].addImage(img);
            } catch (IOException | DocumentException e) {
                logger.error("Wystąpił błąd podczas dodawania tła boiska do tablicy z obenością w danych strefach", e);
            }
        }
    }

    private class PdfEventHandler extends PdfPageEventHelper {
        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            ColumnText.showTextAligned(writer.getDirectContent(),
                    Element.ALIGN_CENTER, new Phrase(String.valueOf(writer.getPageNumber())),
                    document.getPageSize().getWidth() / 2, document.getPageSize().getBottom() + (document.bottomMargin() / 2), 0);
        }
    }
}
