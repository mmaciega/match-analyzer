package pl.mmaciega.matchanalyzer.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import pl.mmaciega.matchanalyzer.model.Player;
import pl.mmaciega.matchanalyzer.model.events.FootballEvent;
import pl.mmaciega.matchanalyzer.stats.Stats;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SheetTable extends PdfPTable {
    private final static String FIRST_COLUMN_HEADER_TEXT = "Imię i nazwisko";

    private Stats stats;
    private String headerData;
    private Font headerFont;
    private List<FootballEvent> events;

    public SheetTable(Stats stats, List<FootballEvent> events) throws IOException, DocumentException {
        super(2);
        this.events = events;
        this.stats = stats;

        headerFont = new Font(BaseFont.createFont("c:/windows/fonts/verdana.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
        headerFont.setColor(BaseColor.WHITE);
    }

    public void setHeaderData(String headerData) {
        this.headerData = headerData;
    }

    public void initialize() throws DocumentException {
        initTable();
        defineHeaders();
        addDataToTable();
    }

    private void initTable() throws DocumentException {
        setHeaderRows(1);
        setSpacingBefore(30);
        setSpacingAfter(20);
        setWidths(new float[]{3, 1.5f});
    }

    private void defineHeaders() {
        getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
        getDefaultCell().setPaddingBottom(6f);

        PdfPCell cell;

        cell = new PdfPCell(new Phrase(FIRST_COLUMN_HEADER_TEXT, headerFont));
        cell.setBackgroundColor(new BaseColor(0, 166, 0));
        cell.setPaddingBottom(6f);
        addCell(cell);

        cell = new PdfPCell(new Phrase(headerData, headerFont));
        cell.setBackgroundColor(new BaseColor(0, 166, 0));
        cell.setPaddingBottom(6f);
        addCell(cell);
    }

    private void addDataToTable() {
        getDefaultCell().setBackgroundColor(null);
        getDefaultCell().setHorizontalAlignment(ALIGN_LEFT);

        fillCellWithData(stats.generateSorted(events));
    }

    private void fillCellWithData(Map<Player, String> data) {
        int i = 0;
        for (Map.Entry<Player, String> entry : data.entrySet()) {
            getDefaultCell().setHorizontalAlignment(ALIGN_LEFT);
            addCell(entry.getKey().getName());

            getDefaultCell().setHorizontalAlignment(ALIGN_CENTER);
            addCell(entry.getValue());

            if (++i == 12) {
                break;
            }
        }
    }
}
