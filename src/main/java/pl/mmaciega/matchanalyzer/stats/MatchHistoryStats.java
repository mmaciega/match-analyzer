package pl.mmaciega.matchanalyzer.stats;

import pl.mmaciega.matchanalyzer.model.MatchHistory;
import pl.mmaciega.matchanalyzer.model.events.FootballEvent;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Mytka on 2015-07-31.
 */
public class MatchHistoryStats {
    private MatchHistory matchHistory;


    public MatchHistoryStats(MatchHistory matchHistory) {
        this.matchHistory = matchHistory;
    }

    public Map<String, String> generateTeamStats() {
        Map<String, String> result = new LinkedHashMap<>();

        Map<FootballEvent.Types, List<FootballEvent>> collectByType = matchHistory.getFootballEvents().stream()
                .collect(Collectors.groupingBy(FootballEvent::getType));

        for (Map.Entry<FootballEvent.Types, List<FootballEvent>> element : collectByType.entrySet()) {
            result.put(element.getKey().getName(), String.valueOf(element.getValue().size()));
        }

        return result;
    }

}
