package pl.mmaciega.matchanalyzer.stats;

import pl.mmaciega.matchanalyzer.model.Player;
import pl.mmaciega.matchanalyzer.model.events.FootballEvent;

import java.awt.*;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by Mytka on 2015-07-31.
 */
public interface Stats {
    Map<Player, String> generateSorted(List<FootballEvent> eventsList);

    default Map<Player, List<FootballEvent>> groupByPlayers(List<FootballEvent> eventsList, Predicate<FootballEvent> predicate) {
        return eventsList
                .stream()
                .filter(predicate)
                .collect(Collectors.groupingBy(FootballEvent::getSourcePlayer));
    }
}
