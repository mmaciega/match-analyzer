package pl.mmaciega.matchanalyzer.stats;

import pl.mmaciega.matchanalyzer.model.Player;
import pl.mmaciega.matchanalyzer.model.events.FootballEvent;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Mytka on 2015-07-31.
 */
public class TypeStats implements Stats {
    private final FootballEvent.Types eventType;

    public TypeStats(FootballEvent.Types eventType) {
        this.eventType = eventType;
    }

    @Override
    public Map<Player, String> generateSorted(List<FootballEvent> eventsList) {
        Map<Player, List<FootballEvent>> playersWithEvents = this.groupByPlayers(eventsList, z -> z.getType().equals(eventType));
        Map<Player, String> sheet = generateFormattedAndSorted(playersWithEvents);
        return sheet;
    }

    private Map<Player, String> generateFormattedAndSorted(Map<Player, List<FootballEvent>> playersWithEvents) {
        return playersWithEvents.entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Comparator.comparing(e -> e.getValue().size())))
                .collect(Collectors.toMap(Map.Entry::getKey, e -> String.valueOf(e.getValue().size()), (e1, e2) -> e1, LinkedHashMap::new));
    }
}
