package pl.mmaciega.matchanalyzer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.mmaciega.matchanalyzer.util.jaxb.wrapers.CountriesListWrapper;
import pl.mmaciega.matchanalyzer.util.jaxb.wrapers.PositionsListWrapper;
import pl.mmaciega.matchanalyzer.model.Team;
import pl.mmaciega.matchanalyzer.util.jaxb.wrapers.MatchHistoryWrapper;
import pl.mmaciega.matchanalyzer.util.jaxb.wrapers.TeamsListWrapper;
import pl.mmaciega.matchanalyzer.util.os.OSFolders;

import javax.xml.bind.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.prefs.Preferences;

public class DataManager {
    private static final String PATH_XML_ISO_COUNTRIES = "/data/ISO_3166-1_countries_pl.xml";
    private static final String PATH_XML_POSITIONS = "/data/positions_pl.xml";
    private static final String FOLDER_PATH_XML_TEAMS = OSFolders.getUserData() + "/Match-analyzer/data";
    private static final String PATH_XML_TEAMS = FOLDER_PATH_XML_TEAMS + "/teams.xml";
    private static final Logger LOGGER = LoggerFactory.getLogger(DataManager.class);
    private static final Preferences PREFERENCES = Preferences.userNodeForPackage(DataManager.class);
    private static final String MATCH_HISTORY_XML_PATH_PREF_KEY = "matchHistoryXmlPath";

    private static List<Team> teams;

    private DataManager() {
    }

    public static void init() {
        initializeCountries();
        initializePositions();
        initializeTeamDatabase();
    }

    private static void initializeCountries() {
        try {
            unmarshalCountriesXmlFile();
        } catch (JAXBException e) {
            LOGGER.warn("Błąd podczas mapowania pliku XML z krajami do obiektów Java", e);
        } catch (NullPointerException e) {
            LOGGER.warn("Błąd podczas wczytywania pliku z krajami", e);
        }
    }

    private static void unmarshalCountriesXmlFile() throws JAXBException, NullPointerException {
        JAXBContext jaxbContext = JAXBContext.newInstance(CountriesListWrapper.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        jaxbUnmarshaller.unmarshal(Main.class.getResourceAsStream(PATH_XML_ISO_COUNTRIES));
    }

    private static void initializePositions() {
        try {
            unmarshalPositionsXmlFile();
        } catch (JAXBException e) {
            LOGGER.warn("Błąd podczas mapowania pliku XML z pozycjami do obiektów Java", e);
        } catch (NullPointerException e) {
            LOGGER.warn("Błąd podczas wczytywania pliku z pozycjami", e);
        }
    }


    private static void unmarshalPositionsXmlFile() throws JAXBException, NullPointerException {
        JAXBContext jaxbContext = JAXBContext.newInstance(PositionsListWrapper.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        jaxbUnmarshaller.unmarshal(Main.class.getResourceAsStream(PATH_XML_POSITIONS));
    }

    private static void initializeTeamDatabase() {
        new File(FOLDER_PATH_XML_TEAMS).mkdir();
        File file = new File(PATH_XML_TEAMS);
        if (file.exists() && !file.isDirectory()) {
            try {
                unmarshalTeamsXmlFile(file);
            } catch (JAXBException e) {
                LOGGER.warn("Błąd podczas mapowania pliku XML z drużynami do obiektów Java", e);
            } catch (NullPointerException e) {
                LOGGER.warn("Błąd podczas wczytywania pliku z drużynami", e);
            }
        }
    }

    private static void unmarshalTeamsXmlFile(File file) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(TeamsListWrapper.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        TeamsListWrapper teamsListWrapper = (TeamsListWrapper) jaxbUnmarshaller.unmarshal(file);
        teams = teamsListWrapper.getTeams();
    }

    public static void saveTeamDatabase(List<Team> teams) {
        try {
            marshalTeamsXmlFile(new File(PATH_XML_TEAMS), teams);
        } catch (JAXBException e) {
            LOGGER.warn("Błąd podczas mapowania obiektów Java na plik XML z drużynami (baza danych)", e);
        } catch (NullPointerException e) {
            LOGGER.warn("Błąd podczas zapisywania pliku z drużynami (baza danych)", e);
        }
    }

    public static void exportTeamDatabase(File file, List<Team> teams) {
        try {
            marshalTeamsXmlFile(file, teams);
        } catch (JAXBException e) {
            LOGGER.warn("Błąd podczas mapowania obiektów Java na plik XML z drużynami (export)", e);
        } catch (NullPointerException e) {
            LOGGER.warn("Błąd podczas zapisywania pliku z drużynami (export)", e);
        }
    }

    public static List<Team> importTeamDatabase(File file) throws JAXBException {
        try {
            unmarshalTeamsXmlFile(file);
            return teams;
        } catch (JAXBException e) {
            LOGGER.warn("Błąd podczas mapowania pliku XML z drużynami do obiektów Java (import bazy danych)", e);
            throw e;
        } catch (NullPointerException e) {
            LOGGER.warn("Błąd podczas wczytywania pliku z drużynami (import bazy danych)", e);
        }

        return null;
    }

    public static void marshalTeamsXmlFile(File file, List<Team> teams) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(TeamsListWrapper.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        TeamsListWrapper teamsListWrapper = new TeamsListWrapper();
        teamsListWrapper.setTeams(teams);

        marshaller.marshal(teamsListWrapper, file);
    }

    public static MatchHistoryWrapper loadMatchHistory(File file) throws JAXBException {
        try {
            MatchHistoryWrapper matchHistoryWrapper = unmarshalMatchHistoryXmlFile(file);
            PREFERENCES.put(MATCH_HISTORY_XML_PATH_PREF_KEY, file.getPath());
            return matchHistoryWrapper;
        } catch (JAXBException e) {
            LOGGER.warn("Błąd podczas mapowania pliku XML z historią meczu do obiektów Java", e);
            throw e;
        }
    }

    public static MatchHistoryWrapper unmarshalMatchHistoryXmlFile(File file) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(MatchHistoryWrapper.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return (MatchHistoryWrapper) jaxbUnmarshaller.unmarshal(file);
    }

    public static void saveMatchHistory(File file, MatchHistoryWrapper matchHistoryWrapper) throws JAXBException {
        try {
            marshalMatchHistoryXmlFile(file, matchHistoryWrapper);
        } catch (JAXBException e) {
            LOGGER.warn("Błąd podczas zapisywania pliku z historią meczu", e);
            throw e;
        }
    }

    public static void saveMatchHistory(MatchHistoryWrapper matchHistoryWrapper) throws JAXBException {
        try {
            String path = PREFERENCES.get(MATCH_HISTORY_XML_PATH_PREF_KEY, null);
            if (path != null) {
                marshalMatchHistoryXmlFile(new File(path), matchHistoryWrapper);
            }
        } catch (JAXBException e) {
            LOGGER.warn("Błąd podczas zapisywania pliku z historią meczu", e);
            throw e;
        }
    }

    private static void marshalMatchHistoryXmlFile(File file, MatchHistoryWrapper matchHistoryWrapper) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(MatchHistoryWrapper.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(matchHistoryWrapper, file);
        PREFERENCES.put(MATCH_HISTORY_XML_PATH_PREF_KEY, file.getPath());
    }

    public static void removeFilePath() {
        PREFERENCES.remove(MATCH_HISTORY_XML_PATH_PREF_KEY);
    }

    public static String getFilePath() {
        return PREFERENCES.get(MATCH_HISTORY_XML_PATH_PREF_KEY, null);
    }

    public static List<Team> getTeams() {
        return teams;
    }
}
