package pl.mmaciega.matchanalyzer.util.jaxb.adapters;

import pl.mmaciega.matchanalyzer.ui.pitch.PlayerGraphic;
import pl.mmaciega.matchanalyzer.util.jaxb.wrapers.PlayerGraphicWrapper;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class PlayerGraphicXmlAdapter extends XmlAdapter<PlayerGraphicWrapper, PlayerGraphic> {
    @Override
    public PlayerGraphic unmarshal(PlayerGraphicWrapper v) throws Exception {
        PlayerGraphic playerGraphic = new PlayerGraphic(v.getPlayer(), v.getPositionGraphic());
        return playerGraphic;
    }

    @Override
    public PlayerGraphicWrapper marshal(PlayerGraphic v) throws Exception {
        PlayerGraphicWrapper playerGraphicWrapper = new PlayerGraphicWrapper();
        playerGraphicWrapper.setPlayer(v.getPlayer());
        playerGraphicWrapper.setPositionGraphic(v.getPositionGraphic());
        return playerGraphicWrapper;

    }
}
