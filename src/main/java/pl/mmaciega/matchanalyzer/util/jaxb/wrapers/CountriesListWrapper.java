package pl.mmaciega.matchanalyzer.util.jaxb.wrapers;

import pl.mmaciega.matchanalyzer.model.Country;

import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@XmlRootElement(name = "countries")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class CountriesListWrapper
{
    private static List<Country> countries = null;

    @XmlElement(name = "country")
    private List<Country> getCountriesXml() {
        return countries;
    }
    private void setCountriesXml(List<Country> countries) {
        CountriesListWrapper.countries = countries;
    }

    public static List<Country> getCountries() {
        return countries;
    }

    public static Optional<Country> findCountryByCode(String code) {
        return  countries.stream().filter(country -> country.getCode().equalsIgnoreCase(code)).findFirst();
    }
}
