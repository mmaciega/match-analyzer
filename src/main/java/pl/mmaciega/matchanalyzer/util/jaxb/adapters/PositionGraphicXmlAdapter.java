package pl.mmaciega.matchanalyzer.util.jaxb.adapters;

import pl.mmaciega.matchanalyzer.controller.PitchController;
import pl.mmaciega.matchanalyzer.ui.pitch.PositionGraphic;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Optional;

public class PositionGraphicXmlAdapter extends XmlAdapter<String, PositionGraphic> {
    @Override
    public PositionGraphic unmarshal(String v) throws Exception {
        Optional<PositionGraphic> positionGraphicOptional = PitchController.getPositionGraphicById(v);
        return positionGraphicOptional.isPresent() ? positionGraphicOptional.get() : null;
    }

    @Override
    public String marshal(PositionGraphic v) throws Exception {
        return v.getText();
    }
}
