package pl.mmaciega.matchanalyzer.util.jaxb.wrapers;

import pl.mmaciega.matchanalyzer.model.Position;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Optional;

@XmlRootElement(name = "positions")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PositionsListWrapper
{
    private static List<Position> positions = null;

    @XmlElement(name = "position")
    private List<Position> getPositionsXml() {
        return positions;
    }
    private void setPositionsXml(List<Position> positions) {
        PositionsListWrapper.positions = positions;
    }

    public static List<Position> getPositions() {
        return positions;
    }

    public static Optional<Position> findPositionByCodePl(String codePl) {
        return  positions.stream().filter(position -> position.getCodePl().equalsIgnoreCase(codePl)).findFirst();
    }
}
