package pl.mmaciega.matchanalyzer.util.jaxb.adapters;

import pl.mmaciega.matchanalyzer.model.Position;
import pl.mmaciega.matchanalyzer.util.jaxb.wrapers.PositionsListWrapper;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Optional;

public class PositionXmlAdapter extends XmlAdapter<String, Position> {
    @Override
    public Position unmarshal(String v) throws Exception {
        Optional<Position> positionOptional = PositionsListWrapper.findPositionByCodePl(v);
        return positionOptional.isPresent() ? positionOptional.get() : null;
    }

    @Override
    public String marshal(Position v) throws Exception {
        return v.getCodePl();
    }
}
