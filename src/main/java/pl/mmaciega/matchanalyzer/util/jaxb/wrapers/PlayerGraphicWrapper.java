package pl.mmaciega.matchanalyzer.util.jaxb.wrapers;

import pl.mmaciega.matchanalyzer.model.Player;
import pl.mmaciega.matchanalyzer.ui.pitch.PositionGraphic;
import pl.mmaciega.matchanalyzer.util.jaxb.adapters.PositionGraphicXmlAdapter;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

public class PlayerGraphicWrapper {
    private Player player;
    private PositionGraphic positionGraphic;

    @XmlAttribute(name = "playerRef")
    @XmlIDREF
    public Player getPlayer() {
        return player;
    }
    public void setPlayer(Player player) {
        this.player = player;
    }

    @XmlAttribute(name = "positionGraphicRef")
    @XmlJavaTypeAdapter(PositionGraphicXmlAdapter.class)
    public PositionGraphic getPositionGraphic() {
        return positionGraphic;
    }
    public void setPositionGraphic(PositionGraphic positionGraphic) {
        this.positionGraphic = positionGraphic;
    }
}
