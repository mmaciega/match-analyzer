package pl.mmaciega.matchanalyzer.util.jaxb.adapters;

import pl.mmaciega.matchanalyzer.model.Country;
import pl.mmaciega.matchanalyzer.util.jaxb.wrapers.CountriesListWrapper;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Optional;

public class CountryXmlAdapter extends XmlAdapter<String, Country> {
    @Override
    public Country unmarshal(String v) throws Exception {
        Optional<Country> countryOptional = CountriesListWrapper.findCountryByCode(v);
        return countryOptional.isPresent() ? countryOptional.get() : null;
    }

    @Override
    public String marshal(Country v) throws Exception {
        return v.getCode();
    }
}
