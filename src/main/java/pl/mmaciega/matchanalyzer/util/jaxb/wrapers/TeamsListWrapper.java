package pl.mmaciega.matchanalyzer.util.jaxb.wrapers;

import pl.mmaciega.matchanalyzer.model.Team;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


@XmlRootElement(name = "teams")
public class TeamsListWrapper {
    private List<Team> teams;

    @XmlElement(name = "team")
    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }
}
