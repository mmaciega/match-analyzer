package pl.mmaciega.matchanalyzer.util.jaxb.adapters;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.awt.image.BufferedImage;

public class ImageXmlAdapter extends XmlAdapter<java.awt.Image, Image> {

    @Override
    public Image unmarshal(java.awt.Image v) throws Exception {
        Image image = null;
        if (v != null) {
            image = SwingFXUtils.toFXImage((BufferedImage) v, null);
        }

        return image;
    }

    @Override
    public java.awt.Image marshal(Image v) throws Exception {
        BufferedImage bufferedImage = null;
        if (v != null) {
            bufferedImage = SwingFXUtils.fromFXImage(v, null);
        }

        return bufferedImage;
    }
}
