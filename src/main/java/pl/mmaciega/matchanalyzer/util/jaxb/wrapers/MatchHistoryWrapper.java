package pl.mmaciega.matchanalyzer.util.jaxb.wrapers;

import javafx.collections.transformation.FilteredList;
import pl.mmaciega.matchanalyzer.model.MatchHistory;
import pl.mmaciega.matchanalyzer.model.Player;
import pl.mmaciega.matchanalyzer.ui.pitch.PlayerGraphic;
import pl.mmaciega.matchanalyzer.util.jaxb.adapters.PlayerGraphicXmlAdapter;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;
import java.util.Optional;

@XmlRootElement(name = "matchHistory")
public class MatchHistoryWrapper {

    private MatchHistory matchHistory;
    private List<PlayerGraphic> playerGraphics;

    @XmlElement(name = "stats")
    public MatchHistory getMatchHistory() {
        return matchHistory;
    }
    public void setMatchHistory(MatchHistory matchHistory) {
        this.matchHistory = matchHistory;
    }

    @XmlElementWrapper(name = "graphics")
    @XmlElement(name = "playerGraphic")
    @XmlJavaTypeAdapter(PlayerGraphicXmlAdapter.class)
    public List<PlayerGraphic> getPlayerGraphics() {
        return playerGraphics;
    }
    public void setPlayerGraphics(List<PlayerGraphic> playerGraphics) {
        this.playerGraphics = playerGraphics;
    }

    public void afterUnmarshal(Unmarshaller u, Object parent) {
        FilteredList<Player> filtered = matchHistory.getSourceTeam().getPlayers().filtered(player -> {
            Optional<PlayerGraphic> playerGraphicOptional = playerGraphics.stream()
                    .filter(playerGraphic -> playerGraphic.getPlayer() == player)
                    .findFirst();
            return playerGraphicOptional.isPresent() ? true : false;
        });

        matchHistory.setFirstSquad(filtered);
    }
}
