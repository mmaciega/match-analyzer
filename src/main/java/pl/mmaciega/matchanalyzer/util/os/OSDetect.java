package pl.mmaciega.matchanalyzer.util.os;

/**
 * Created by Mytka on 2015-07-23.
 */
public class OSDetect {

    // ~ Enumerations --------------------------------------------------------------------------------------------------

    public enum Platform {
        WIN32, WIN64, MAC32, MAC64, LINUX32, LINUX64
    }

    public static final Platform	PLATFORM;

    static {
        String bitage = System.getProperty("sun.arch.data.model");
        boolean bit64 = false;
        if (bitage.equals("32")) {
            bit64 = false;
        } else if (bitage.equals("64")) {
            bit64 = true;
        }

        String os = System.getProperty("os.name").toLowerCase();
        if (os.indexOf("win") != -1) {
            PLATFORM = (bit64 ? Platform.WIN64 : Platform.WIN32);
        } else if (os.indexOf("mac") != -1) {
            PLATFORM = (bit64 ? Platform.MAC64 : Platform.MAC32);
        } else if (os.indexOf("linux") != -1) {
            PLATFORM = (bit64 ? Platform.LINUX64 : Platform.LINUX32);
        } else {
            PLATFORM = null;
        }
    }

    public static boolean isWindows() {
        return ((PLATFORM == Platform.WIN32) || (PLATFORM == Platform.WIN64));
    }

    public static boolean isMac() {
        return ((PLATFORM == Platform.MAC32) || (PLATFORM == Platform.MAC64));
    }

    public static boolean isLinux() {
        return ((PLATFORM == Platform.LINUX32) || (PLATFORM == Platform.LINUX64));
    }
}