package pl.mmaciega.matchanalyzer.util.os;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.PointerType;
import com.sun.jna.win32.W32APIFunctionMapper;
import com.sun.jna.win32.W32APITypeMapper;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mytka on 2015-07-23.
 */
public class OSFolders {

    // ~ Enumerations --------------------------------------------------------------------------------------------------

    private enum Folder {
        USER_DOCS, USER_DATA
    }

    public static String getUserData() {
        switch (OSDetect.PLATFORM) {
            case LINUX32:
            case LINUX64:
                return getFolderLinux(Folder.USER_DATA) + File.separator;
            case MAC32:
            case MAC64:
                return getFolderMac(Folder.USER_DATA) + File.separator;
            case WIN32:
            case WIN64:
                return getFolderWindows(Folder.USER_DATA) + File.separator;
            default:
                return null;
        }
    }

    public static String getUserDocuments() {
        switch (OSDetect.PLATFORM) {
            case LINUX32:
            case LINUX64:
                return getFolderLinux(Folder.USER_DOCS) + File.separator;
            case MAC32:
            case MAC64:
                return getFolderMac(Folder.USER_DOCS) + File.separator;
            case WIN32:
            case WIN64:
                return getFolderWindows(Folder.USER_DOCS) + File.separator;
            default:
                return null;
        }
    }

    private static String getFolderLinux(final Folder folderType) {
        String userHome = System.getProperty("user.home");

        switch (folderType) {
            case USER_DATA:
                String folder = System.getenv("XDG_DATA_HOME");
                if (folder == null) {
                    folder = userHome + File.separator + ".local" + File.separator + "share";
                }
                return folder;
            case USER_DOCS:
                return userHome;
            default:
                return null;
        }
    }

    private static String getFolderMac(final Folder folderType) {
        String userHome = System.getProperty("user.home");

        switch (folderType) {
            case USER_DATA:
                return userHome + File.separator + "Library" + File.separator + "Application Support";
            case USER_DOCS:
                return userHome + File.separator + "Documents";
            default:
                return null;
        }
    }

    private static String getFolderWindows(final Folder folderType) {

        Map<String, Object> options = new HashMap<>();
        options.put(Library.OPTION_TYPE_MAPPER, W32APITypeMapper.UNICODE);
        options.put(Library.OPTION_FUNCTION_MAPPER, W32APIFunctionMapper.UNICODE);

        int nFolder;
        switch (folderType) {
            case USER_DATA:
                nFolder = Shell32.CSIDL_LOCAL_APPDATA;
                break;
            case USER_DOCS:
                nFolder = Shell32.CSIDL_PERSONAL;
                break;
            default:
                return null;
        }
        HWND hwndOwner = null;
        HANDLE hToken = null;
        int dwFlags = Shell32.SHGFP_TYPE_CURRENT;
        char pszPath[] = new char[Shell32.MAX_PATH];
        Shell32 instance = (Shell32) Native.loadLibrary("shell32", Shell32.class, options);
        int hResult = instance.SHGetFolderPath(hwndOwner, nFolder, hToken, dwFlags, pszPath);
        if (Shell32.S_OK == hResult) {

            String path = new String(pszPath);
            int len = path.indexOf('\0');
            return path.substring(0, len);
        } else {
            return "C:\\";
        }
    }

    // ~ Inner Classes -------------------------------------------------------------------------------------------------

    private static class HANDLE extends PointerType {
    }

    private static class HWND extends HANDLE {
    }

    // ~ Inner Interfaces ----------------------------------------------------------------------------------------------

    private interface Shell32 extends Library {

        int	MAX_PATH			= 260;
        int	CSIDL_LOCAL_APPDATA	= 0x001c;
        int	CSIDL_PERSONAL		= 0x0005;
        int	SHGFP_TYPE_CURRENT	= 0;
        @SuppressWarnings("unused")
        int	SHGFP_TYPE_DEFAULT	= 1;
        int	S_OK				= 0;

        /**
         * see http://msdn.microsoft.com/en-us/library/bb762181(VS.85).aspx
         *
         * HRESULT SHGetFolderPath( HWND hwndOwner, int nFolder, HANDLE hToken, DWORD dwFlags, LPTSTR pszPath);
         */
        int SHGetFolderPath(final HWND hwndOwner, final int nFolder, final HANDLE hToken, final int dwFlags,
                            final char pszPath[]);
    }
}
