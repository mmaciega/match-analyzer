package pl.mmaciega.matchanalyzer.util;

import javafx.scene.image.Image;

import java.io.InputStream;

public class ResourceFileManager {
    public static Image getImage(String imagePath) {
        InputStream resource = getResource("/images/" + imagePath);
        return resource != null ? new Image(resource) : null;
    }

    private static InputStream getResource(String resource) {
        return ResourceFileManager.class.getResourceAsStream(resource);
    }
    

}