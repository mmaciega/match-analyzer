package pl.mmaciega.matchanalyzer.ui;

import com.sun.jna.Memory;
import javafx.application.Platform;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.*;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import uk.co.caprica.vlcj.binding.LibVlc;
import uk.co.caprica.vlcj.binding.internal.libvlc_media_t;
import uk.co.caprica.vlcj.component.DirectMediaPlayerComponent;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventListener;
import uk.co.caprica.vlcj.player.direct.BufferFormat;
import uk.co.caprica.vlcj.player.direct.BufferFormatCallback;
import uk.co.caprica.vlcj.player.direct.DirectMediaPlayer;
import uk.co.caprica.vlcj.player.direct.format.RV32BufferFormat;

import java.nio.ByteBuffer;

public class ResizableMediaPlayer {
    private DirectMediaPlayerComponent mediaPlayerComponent;
    private WritableImage writableImage;
    private WritablePixelFormat<ByteBuffer> pixelFormat;
    private FloatProperty videoSourceRatioProperty;
    private ImageView imageView;

    private ResizableMediaPlayer() {
        mediaPlayerComponent = new CanvasPlayerComponent();
        mediaPlayerComponent.getMediaPlayer().addMediaPlayerEventListener(new MediaPlayerEventListener() {
            @Override
            public void mediaChanged(MediaPlayer mediaPlayer, libvlc_media_t media, String mrl) {
            }

            @Override
            public void opening(MediaPlayer mediaPlayer) {

            }

            @Override
            public void buffering(MediaPlayer mediaPlayer, float newCache) {

            }

            @Override
            public void playing(MediaPlayer mediaPlayer) {

            }

            @Override
            public void paused(MediaPlayer mediaPlayer) {

            }

            @Override
            public void stopped(MediaPlayer mediaPlayer) {
                Platform.runLater(() -> imageView.setVisible(false));
            }

            @Override
            public void forward(MediaPlayer mediaPlayer) {

            }

            @Override
            public void backward(MediaPlayer mediaPlayer) {

            }

            @Override
            public void finished(MediaPlayer mediaPlayer) {

            }

            @Override
            public void timeChanged(MediaPlayer mediaPlayer, long newTime) {

            }

            @Override
            public void positionChanged(MediaPlayer mediaPlayer, float newPosition) {

            }

            @Override
            public void seekableChanged(MediaPlayer mediaPlayer, int newSeekable) {

            }

            @Override
            public void pausableChanged(MediaPlayer mediaPlayer, int newPausable) {

            }

            @Override
            public void titleChanged(MediaPlayer mediaPlayer, int newTitle) {

            }

            @Override
            public void snapshotTaken(MediaPlayer mediaPlayer, String filename) {

            }

            @Override
            public void lengthChanged(MediaPlayer mediaPlayer, long newLength) {

            }

            @Override
            public void videoOutput(MediaPlayer mediaPlayer, int newCount) {
                Platform.runLater(() -> imageView.setVisible(true));
            }

            @Override
            public void scrambledChanged(MediaPlayer mediaPlayer, int newScrambled) {

            }

            @Override
            public void elementaryStreamAdded(MediaPlayer mediaPlayer, int type, int id) {

            }

            @Override
            public void elementaryStreamDeleted(MediaPlayer mediaPlayer, int type, int id) {

            }

            @Override
            public void elementaryStreamSelected(MediaPlayer mediaPlayer, int type, int id) {

            }

            @Override
            public void error(MediaPlayer mediaPlayer) {
                Platform.runLater(() -> imageView.setVisible(false));
            }

            @Override
            public void mediaMetaChanged(MediaPlayer mediaPlayer, int metaType) {

            }

            @Override
            public void mediaSubItemAdded(MediaPlayer mediaPlayer, libvlc_media_t subItem) {

            }

            @Override
            public void mediaDurationChanged(MediaPlayer mediaPlayer, long newDuration) {

            }

            @Override
            public void mediaParsedChanged(MediaPlayer mediaPlayer, int newStatus) {

            }

            @Override
            public void mediaFreed(MediaPlayer mediaPlayer) {

            }

            @Override
            public void mediaStateChanged(MediaPlayer mediaPlayer, int newState) {

            }

            @Override
            public void mediaSubItemTreeAdded(MediaPlayer mediaPlayer, libvlc_media_t item) {

            }

            @Override
            public void newMedia(MediaPlayer mediaPlayer) {
            }

            @Override
            public void subItemPlayed(MediaPlayer mediaPlayer, int subItemIndex) {

            }

            @Override
            public void subItemFinished(MediaPlayer mediaPlayer, int subItemIndex) {

            }

            @Override
            public void endOfSubItems(MediaPlayer mediaPlayer) {

            }
        });
        videoSourceRatioProperty = new SimpleFloatProperty(0.4f);
        pixelFormat = PixelFormat.getByteBgraPreInstance();
    }

    public void init(Pane playerHolder) {
        initializeImageView(playerHolder);
    }

    private void initializeImageView(Pane playerHolder) {
        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
        writableImage = new WritableImage((int) visualBounds.getWidth(), (int) visualBounds.getHeight());

        imageView = new ImageView(writableImage);
        playerHolder.getChildren().add(imageView);

        playerHolder.widthProperty().addListener((observable, oldValue, newValue) ->
                        fitImageViewSize(newValue.floatValue(), (float) playerHolder.getHeight())
        );

        playerHolder.heightProperty().addListener((observable, oldValue, newValue) ->
                        fitImageViewSize((float) playerHolder.getWidth(), newValue.floatValue())
        );

        videoSourceRatioProperty.addListener((observable, oldValue, newValue) ->
                        fitImageViewSize((float) playerHolder.getWidth(), (float) playerHolder.getHeight())
        );
    }

    private void fitImageViewSize(float width, float height) {
        Platform.runLater(() -> {
            float fitHeight = videoSourceRatioProperty.get() * width;
            if (fitHeight > height) {
                imageView.setFitHeight(height);
                double fitWidth = height / videoSourceRatioProperty.get();
                imageView.setFitWidth(fitWidth);
                imageView.setX((width - fitWidth) / 2);
                imageView.setY(0);
            } else {
                imageView.setFitWidth(width);
                imageView.setFitHeight(fitHeight);
                imageView.setY((height - fitHeight) / 2);
                imageView.setX(0);
            }
        });
    }

    private static ResizableMediaPlayer instance = null;

    public static ResizableMediaPlayer getInstance() {
        if (instance == null) {
            instance = new ResizableMediaPlayer();
        }
        return instance;
    }

    public static void loadNativeLibraries() throws Throwable {
        new NativeDiscovery().discover();

        try {
            System.out.println(LibVlc.INSTANCE.libvlc_get_version());
        } catch (UnsatisfiedLinkError e) {
            throw e;
        }

    }

    public void release() {
        mediaPlayerComponent.release();
    }

    public DirectMediaPlayer getMediaPlayer() {
        return mediaPlayerComponent.getMediaPlayer();
    }

    private class CanvasPlayerComponent extends DirectMediaPlayerComponent {

        public CanvasPlayerComponent() {
            super(new CanvasBufferFormatCallback());
        }

        PixelWriter pixelWriter = null;

        private PixelWriter getPixelWriter() {
            if (pixelWriter == null) {
                pixelWriter = writableImage.getPixelWriter();
            }
            return pixelWriter;
        }

        @Override
        public void display(DirectMediaPlayer mediaPlayer, Memory[] nativeBuffers, BufferFormat bufferFormat) {
            if (writableImage == null) {
                return;
            }

            Platform.runLater(() -> renderFrame(mediaPlayer, bufferFormat));
        }

        private void renderFrame(DirectMediaPlayer mediaPlayer, BufferFormat bufferFormat) {
            Memory[] buffer = mediaPlayer.lock();

            try {
                if (buffer != null) {
                    Memory nativeBuffer = buffer[0];

                    ByteBuffer byteBuffer = nativeBuffer.getByteBuffer(0, nativeBuffer.size());
                    getPixelWriter().setPixels(0, 0, bufferFormat.getWidth(), bufferFormat.getHeight(), pixelFormat, byteBuffer, bufferFormat.getPitches()[0]);

                }
            } finally {
                mediaPlayer.unlock();
            }
        }
    }

    private class CanvasBufferFormatCallback implements BufferFormatCallback {
        @Override
        public BufferFormat getBufferFormat(int sourceWidth, int sourceHeight) {
            Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
            Platform.runLater(() -> videoSourceRatioProperty.set((float) sourceHeight / (float) sourceWidth));
            return new RV32BufferFormat((int) visualBounds.getWidth(), (int) visualBounds.getHeight());
        }
    }
}
