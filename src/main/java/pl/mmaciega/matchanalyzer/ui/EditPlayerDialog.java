package pl.mmaciega.matchanalyzer.ui;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;
import pl.mmaciega.matchanalyzer.Main;
import pl.mmaciega.matchanalyzer.model.Country;
import pl.mmaciega.matchanalyzer.model.Player;
import pl.mmaciega.matchanalyzer.model.Position;
import pl.mmaciega.matchanalyzer.util.jaxb.wrapers.CountriesListWrapper;
import pl.mmaciega.matchanalyzer.util.jaxb.wrapers.PositionsListWrapper;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class EditPlayerDialog extends Dialog<Player> {
    @FXML
    private ComboBox<Integer> numberComboBox;
    @FXML
    private ComboBox<Position> positionComboBox;
    @FXML
    private ComboBox<Country> countryComboBox;
    @FXML
    private TextField playerName;

    private Player selectedPlayer;

    public EditPlayerDialog(Player selectedPlayer) throws IOException {
        super();
        this.selectedPlayer = selectedPlayer;

        setTitle("Edytuj zawodnika");
        setHeaderText("Zmodyfikuj informacje w celu edytowania zawodnika");

        GridPane gridPane = loadDialogPane();

        ButtonType editButtonType = new ButtonType("Edytuj", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancelButtonType = new ButtonType("Anuluj", ButtonBar.ButtonData.CANCEL_CLOSE);
        getDialogPane().getButtonTypes().addAll(editButtonType, cancelButtonType);
        getDialogPane().setContent(gridPane);

        Node createButton = getDialogPane().lookupButton(editButtonType);
        BooleanBinding booleanBinding = Bindings.createBooleanBinding(() -> {
            Optional<Player> playerOptional = selectedPlayer.getTeam().getPlayers().stream().filter(player -> player.getName().equalsIgnoreCase(playerName.getText()) && player != selectedPlayer).findFirst();
            return playerOptional.isPresent() ? true : false;
        }, playerName.textProperty());
        createButton.disableProperty().bind(playerName.textProperty().isEmpty().or(booleanBinding.or(countryComboBox.valueProperty().isNull()).or(numberComboBox.valueProperty().isNull()).or(positionComboBox.valueProperty().isNull())));

        Platform.runLater(playerName::requestFocus);

        setResultConverter(dialogButton -> {
            if (dialogButton == editButtonType) {
                return new Player(playerName.getText(),
                        numberComboBox.getSelectionModel().getSelectedItem(),
                        countryComboBox.getSelectionModel().getSelectedItem(),
                        positionComboBox.getSelectionModel().getSelectedItem(),
                        selectedPlayer.getTeam());
            }
            return null;
        });
    }

    private GridPane loadDialogPane() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(Main.class.getResource("/view/edit/EditPlayer.fxml"));
        fxmlLoader.setController(this);
        return fxmlLoader.load();
    }

    @FXML
    private void initialize() {
        playerName.setText(selectedPlayer.getName());

        List<Integer> reservedNumbers = selectedPlayer.getTeam().getPlayers().stream()
                .mapToInt(Player::getNumber)
                .boxed()
                .collect(Collectors.toList());
        numberComboBox.setItems(FXCollections.observableArrayList(
                IntStream
                        .range(1, 100)
                        .filter(value -> value == selectedPlayer.getNumber() || !reservedNumbers.contains(value))
                        .boxed()
                        .collect(Collectors.toList())
        ));
        numberComboBox.getSelectionModel().select((Integer) selectedPlayer.getNumber());
        new SelectKeyComboBoxListener(numberComboBox);

        countryComboBox.setCellFactory(new Callback<ListView<Country>, ListCell<Country>>() {
            @Override
            public ListCell<Country> call(ListView<Country> param) {
                return new ListCell<Country>() {
                    @Override
                    protected void updateItem(Country item, boolean empty) {
                        super.updateItem(item, empty);

                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            setText(item.getNamePl());
                            setGraphic(new ImageView(item.getImageFlag()));
                        }
                    }
                };
            }
        });
        countryComboBox.setButtonCell(new ListCell() {
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    Country country = (Country) item;

                    setText(country.getNamePl());
                    setGraphic(new ImageView(country.getImageFlag()));
                }
            }
        });
        countryComboBox.setItems(FXCollections.observableArrayList(CountriesListWrapper.getCountries()));
        countryComboBox.getSelectionModel().select(selectedPlayer.getCountry());
        new SelectKeyComboBoxListener(countryComboBox);

        positionComboBox.setCellFactory(param -> new ListCell<Position>() {
            @Override
            protected void updateItem(Position item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    setText(item.getNamePl() + " (" + item.getCodePl() + ")");
                }
            }
        });
        positionComboBox.setButtonCell(new ListCell() {
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null) {
                    setText(null);
                    setGraphic(null);
                } else {
                    Position position = (Position) item;
                    setText(position.getNamePl() + " (" + position.getCodePl() + ")");
                }
            }
        });
        positionComboBox.setItems(FXCollections.observableArrayList(PositionsListWrapper.getPositions()));
        positionComboBox.getSelectionModel().select(selectedPlayer.getPosition());
        new SelectKeyComboBoxListener(positionComboBox);
    }
}
