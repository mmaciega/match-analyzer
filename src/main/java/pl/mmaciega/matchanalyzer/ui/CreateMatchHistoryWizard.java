package pl.mmaciega.matchanalyzer.ui;

import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Window;
import javafx.util.Callback;
import org.controlsfx.control.ListSelectionView;
import org.controlsfx.dialog.Wizard;
import org.controlsfx.dialog.WizardPane;
import org.controlsfx.validation.ValidationSupport;
import pl.mmaciega.matchanalyzer.Main;
import pl.mmaciega.matchanalyzer.model.*;
import pl.mmaciega.matchanalyzer.ui.pitch.PositionGraphic;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class CreateMatchHistoryWizard extends Wizard {
    ObservableList<Team> teams;
    ObservableList<PositionGraphic> positions;

    public CreateMatchHistoryWizard(Window owner, ObservableList<Team> teams, List<PositionGraphic> positions) throws IOException {
        super(owner);

        this.teams = teams;
        this.positions = FXCollections.observableArrayList(positions);
        setTitle("Stworz nową historię meczu");

        TeamDetailsWizardPane firstPage = new TeamDetailsWizardPane();
//        FirstSquadWizardPane secondPage = new FirstSquadWizardPane();
        FirstSquadSelectionWizardPane secondPage = new FirstSquadSelectionWizardPane();
//        invalidProperty().bind(firstPage.validationSupport.invalidProperty());
        setFlow(new LinearFlow(firstPage, secondPage));
    }

    private class TeamDetailsWizardPane extends WizardPane {
        @FXML
        private ComboBox<Team> hostComboBox;
        @FXML
        private ComboBox<Team> guestComboBox;
        @FXML
        private ToggleGroup eventsSource;
        @FXML
        private DatePicker matchDate;

        ValidationSupport validationSupport;

        public TeamDetailsWizardPane() throws IOException {
            setHeaderText("Zdefiniuj informacje o nowej historii meczu");
            System.out.println(getScene());

            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(Main.class.getResource("/view/edit/NewHistory.fxml"));
            fxmlLoader.setController(this);
            GridPane gridPane = fxmlLoader.load();
            setContent(gridPane);
        }

        @Override
        public void onEnteringPage(Wizard wizard) {
            invalidProperty().bind(hostComboBox.valueProperty().isNull().or(guestComboBox.valueProperty().isNull()).or(eventsSource.selectedToggleProperty().isNull()).or(Bindings.createBooleanBinding(() -> matchDate.getValue() != null && matchDate.getValue().isAfter(LocalDate.now()), matchDate.valueProperty())));
        }

        @Override
        public void onExitingPage(Wizard wizard) {
            invalidProperty().unbind();
            RadioButton selectedRadioButton = (RadioButton) eventsSource.getSelectedToggle();
            getSettings().put("players", selectedRadioButton.getId().equals("hostRadioButton") ? hostComboBox.getValue().getPlayers() : guestComboBox.getValue().getPlayers());

            getSettings().put("hostTeam", hostComboBox.getValue());
            getSettings().put("guestTeam", guestComboBox.getValue());
            getSettings().put("sourceTeam", selectedRadioButton.getId().equals("hostRadioButton") ? hostComboBox.getValue() : guestComboBox.getValue());
            getSettings().put("matchDate", matchDate.getValue());
        }

        @FXML
        private void initialize() {
            SortedList<Team> sortedTeams = teams.sorted((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
            Predicate<Team> teamPredicate = team -> hostComboBox.getValue() != team;
            FilteredList<Team> filtered = sortedTeams.filtered(teamPredicate);

            hostComboBox.setItems(sortedTeams);
            hostComboBox.setCellFactory(param -> new ListCell<Team>() {
                @Override
                protected void updateItem(Team item, boolean empty) {
                    super.updateItem(item, empty);

                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        setText(item.getName());
                        setGraphic(new ImageView(item.getCountry().getImageFlag()));
                    }
                }
            });
            hostComboBox.setButtonCell(new ListCell<Team>() {
                @Override
                protected void updateItem(Team item, boolean empty) {
                    super.updateItem(item, empty);

                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        setText(item.getName());
                        setGraphic(new ImageView(item.getCountry().getImageFlag()));
                    }
                }
            });
            hostComboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                filtered.setPredicate(team -> newValue != team);
            });
            new SelectKeyComboBoxListener(hostComboBox);

            guestComboBox.setItems(filtered);
            guestComboBox.setCellFactory(param -> new ListCell<Team>() {
                @Override
                protected void updateItem(Team item, boolean empty) {
                    super.updateItem(item, empty);

                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        setText(item.getName());
                        setGraphic(new ImageView(item.getCountry().getImageFlag()));
                    }
                }
            });
            guestComboBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue == null) {
                    guestComboBox.valueProperty().set(null);
                }
            });
            guestComboBox.setButtonCell(new ListCell<Team>() {
                @Override
                protected void updateItem(Team item, boolean empty) {
                    super.updateItem(item, empty);

                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        setText(item.getName());
                        setGraphic(new ImageView(item.getCountry().getImageFlag()));
                    }
                }
            });
            new SelectKeyComboBoxListener(guestComboBox);
        }
    }

    private class FirstSquadWizardPane extends WizardPane {
        @FXML
        private TableView<Team> playersTable;
        @FXML
        private TableColumn<Player, PositionGraphic> setColumn;
        @FXML
        private TableColumn<Player, Number> numberColumn;
        @FXML
        private TableColumn<Player, Country> countryColumn;
        @FXML
        private TableColumn<Player, Position> positionColumn;
        @FXML
        private TableColumn<Player, String> nameColumn;
        @FXML
        private Label leftToAssignLabel;
        private IntegerProperty leftToAssign = new SimpleIntegerProperty(11);

        private ObservableList<PositionGraphic> selectedPositionGraphics = FXCollections.observableArrayList();

        public FirstSquadWizardPane() throws IOException {
            setHeaderText("Ustaw pozycje na boisku dla pierwszego składu");
            getStylesheets().add(getClass().getResource("/css/Main.css").toExternalForm());

            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(Main.class.getResource("/view/edit/FirstSquad.fxml"));
            fxmlLoader.setController(this);
            GridPane gridPane = fxmlLoader.load();
            setContent(gridPane);
        }

        @FXML
        private void initialize() {
            leftToAssignLabel.textProperty().bind(leftToAssign.asString());

            playersTable.setPlaceholder(new Label("Brak zawodników w drużynie"));

            setColumn.setCellValueFactory(cellData -> cellData.getValue() != null ? new SimpleObjectProperty<>() : null);
            setColumn.setCellFactory(param1 -> new ComboBoxPositionsCell());
            numberColumn.setCellValueFactory(cellData -> cellData.getValue().numberProperty());
            numberColumn.setCellFactory(param -> new TableCell<Player, Number>() {
                {
                    setAlignment(Pos.CENTER);
                }

                @Override
                protected void updateItem(Number item, boolean empty) {
                    super.updateItem(item, empty);

                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        setText(item.toString());
                    }
                }
            });
            countryColumn.setCellValueFactory(cellData -> cellData.getValue().countryProperty());
            countryColumn.setCellFactory(param -> new TableCell<Player, Country>() {
                {
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    setAlignment(Pos.CENTER);
                }

                @Override
                protected void updateItem(Country item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item != null && !empty) {
                        ImageView imageView = new ImageView();
                        imageView.setFitHeight(20);
                        imageView.setFitWidth(20);
                        imageView.setImage(item.getImageFlag());
                        setGraphic(imageView);
                        setTooltip(new Tooltip(item.getNamePl()));
                    } else {
                        setGraphic(null);
                        setTooltip(null);
                    }
                }
            });
            positionColumn.setCellValueFactory(cellData -> cellData.getValue().positionProperty());
            positionColumn.setCellFactory(new Callback<TableColumn<Player, Position>, TableCell<Player, Position>>() {
                @Override
                public TableCell<Player, Position> call(TableColumn<Player, Position> param) {
                    return new TableCell<Player, Position>() {
                        {
                            setAlignment(Pos.CENTER);
                        }

                        @Override
                        protected void updateItem(Position item, boolean empty) {
                            super.updateItem(item, empty);

                            if (empty || item == null) {
                                setText(null);
                                setGraphic(null);
                                setTooltip(null);
                            } else {
                                setText(item.getCodePl());
                                setTooltip(new Tooltip(item.getNamePl()));
                            }
                        }
                    };
                }
            });
            nameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        }

        @Override
        public void onEnteringPage(Wizard wizard) {
            playersTable.setItems((ObservableList<Team>) wizard.getSettings().get("players"));

            Button button = (Button) lookupButton(ButtonType.FINISH);
            button.disableProperty().bind(leftToAssign.isNotEqualTo(0));
            button = (Button) lookupButton(ButtonType.CANCEL);
            button.setText("Anuluj");

        }

        private class ComboBoxPositionsCell extends TableCell<Player, PositionGraphic> {
            private final String noItem = "---";
            private ObservableList<PositionGraphic> originalItems = FXCollections.observableArrayList();
            private ComboBox<PositionGraphic> comboBox;

            private ComboBoxPositionsCell() {
                comboBox = new ComboBox<>();
                comboBox.disableProperty().bind(Bindings.when(leftToAssign.isEqualTo(0).and(itemProperty().isNull())).then(true).otherwise(false));
                PositionGraphic nullPosition = new PositionGraphic(new Position(-1, null, null, null, null));
                originalItems.add(nullPosition);
                originalItems.addAll(CreateMatchHistoryWizard.this.positions);

                selectedPositionGraphics.addListener((ListChangeListener<? super PositionGraphic>) c -> {
                    while (c.next()) {
                        for (Object remitem : c.getRemoved()) {
                            PositionGraphic positionGraphic = (PositionGraphic) remitem;
                            originalItems.add(positionGraphic);
                        }
                        for (Object additem : c.getAddedSubList()) {
                            PositionGraphic positionGraphic = (PositionGraphic) additem;
                            if (positionGraphic != comboBox.getValue()) {
                                originalItems.remove(positionGraphic);
                            }
                        }
                    }
                });


                System.out.println("Original list " + originalItems + " " + System.identityHashCode(originalItems));
                comboBox.setItems(new SortedList<>(originalItems, Comparator.comparingInt(value -> value.getPosition().getId())));
//                comboBox.setItems(originalItems);
                comboBox.getSelectionModel().select(0);
                comboBox.setCellFactory(param -> new ListCell<PositionGraphic>() {
                    @Override
                    protected void updateItem(PositionGraphic item, boolean empty) {
                        super.updateItem(item, empty);

                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                        } else if (item.getPosition().getId() == -1) {
                            setText(noItem);
                            setGraphic(null);
                        } else {
                            setText(item.getPosition().getCodePl().toUpperCase());
                            setGraphic(null);
                        }
                    }
                });
                comboBox.setButtonCell(new ListCell<PositionGraphic>() {
                    @Override
                    protected void updateItem(PositionGraphic item, boolean empty) {
                        super.updateItem(item, empty);

                        if (empty || item == null) {
                            setText(null);
                            setGraphic(null);
                        } else if (item.getPosition().getId() == -1) {
                            setText(noItem);
                            setGraphic(null);
                        } else {
                            setText(item.getPosition().getCodePl().toUpperCase());
                            setGraphic(null);
                        }
                    }
                });
                comboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
                    if (newValue.getPosition().getId() == -1) {
                        setItem(null);
                        leftToAssign.setValue(leftToAssign.getValue() + 1);
                        System.out.println("Zmiana combox na " + newValue + " item " + getItem() + " - " + getTableView().getItems().get(this.getIndex()).getName());
                    } else if (newValue.getPosition() != null) {
                        setItem(newValue);
                        leftToAssign.setValue(leftToAssign.getValue() - 1);
                        selectedPositionGraphics.add(newValue);
                        System.out.println("Zmiana combox na " + newValue + " item " + getItem().getText() + " - " + getTableView().getItems().get(this.getIndex()).getName());
                    }

                    selectedPositionGraphics.remove(oldValue);
                });

                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setAlignment(Pos.CENTER);
            }

            @Override
            protected void updateItem(PositionGraphic item, boolean empty) {
                super.updateItem(item, empty);

                if (!empty) {
                    setText(null);
                    setGraphic(comboBox);
                }
            }
        }

//        class ComboBoxEditingCell extends TableCell<Player, PositionGraphic> {
//
//            private ComboBox<PositionGraphic> comboBox;
//
//            private ComboBoxEditingCell() {
//            }
//
//            @Override
//            public void startEdit() {
//                if (!isEmpty()) {
//                    super.startEdit();
//                    createComboBox();
//                    setText(null);
//                    setGraphic(comboBox);
//                }
//            }
//
//            @Override
//            public void cancelEdit() {
//                super.cancelEdit();
//
//                setText(getTyp().getTyp());
//                setGraphic(null);
//            }
//
//            @Override
//            public void updateItem(Typ item, boolean empty) {
//                super.updateItem(item, empty);
//
//                if (empty) {
//                    setText(null);
//                    setGraphic(null);
//                } else {
//                    if (isEditing()) {
//                        if (comboBox != null) {
//                            comboBox.setValue(getTyp());
//                        }
//                        setText(getTyp().getTyp());
//                        setGraphic(comboBox);
//                    } else {
//                        setText(getTyp().getTyp());
//                        setGraphic(null);
//                    }
//                }
//            }
//
//            private void createComboBox() {
//                comboBox = new ComboBox<>(typData);
//                comboBoxConverter(comboBox);
//                comboBox.valueProperty().set(getTyp());
//                comboBox.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
//                comboBox.setOnAction((noItem) -> {
//                    System.out.println("Committed: " + comboBox.getSelectionModel().getSelectedItem());
//                    commitEdit(comboBox.getSelectionModel().getSelectedItem());
//                });
////            comboBox.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
////                if (!newValue) {
////                    commitEdit(comboBox.getSelectionModel().getSelectedItem());
////                }
////            });
//            }
//
//            private void comboBoxConverter(ComboBox<Typ> comboBox) {
//                // Define rendering of the list of values in ComboBox drop down.
//                comboBox.setCellFactory((c) -> {
//                    return new ListCell<Typ>() {
//                        @Override
//                        protected void updateItem(Typ item, boolean empty) {
//                            super.updateItem(item, empty);
//
//                            if (item == null || empty) {
//                                setText(null);
//                            } else {
//                                setText(item.getTyp());
//                            }
//                        }
//                    };
//                });
//            }
//
//            private Typ getTyp() {
//                return getItem() == null ? new Typ("") : getItem();
//            }
//        }
    }

    private class FirstSquadSelectionWizardPane extends WizardPane {
        @FXML
        private ListSelectionView<Player> selectionView;
        @FXML
        private Label leftToAssignLabel;
        private IntegerProperty leftToAssign = new SimpleIntegerProperty(11);

        public FirstSquadSelectionWizardPane() throws IOException {
            setHeaderText("Wybierz zawodników do pierwszego składu");
            getStylesheets().add(getClass().getResource("/css/Main.css").toExternalForm());

            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(Main.class.getResource("/view/edit/FirstSquadSelection.fxml"));
            fxmlLoader.setController(this);
            GridPane gridPane = fxmlLoader.load();
            setContent(gridPane);
        }

        @FXML
        private void initialize() {
            leftToAssignLabel.textProperty().bind(leftToAssign.asString());
            leftToAssignLabel.textFillProperty().bind(Bindings.when(leftToAssign.lessThan(0)).then(Color.RED).otherwise(Color.BLACK));
            leftToAssign.bind(Bindings.subtract(11, Bindings.size(selectionView.getTargetItems())));

            selectionView.setCellFactory(param -> new ListCell<Player>() {
                ImageView imageView = new ImageView();

                {
                    imageView.setFitHeight(20);
                    imageView.setFitWidth(20);
                }

                @Override
                protected void updateItem(Player item, boolean empty) {
                    super.updateItem(item, empty);

                    if (empty || item == null) {
                        setText(null);
                        setGraphic(null);
                    } else {
                        imageView.setImage(getItem().getCountry().getImageFlag());
                        setGraphic(imageView);
                        setText(item.getNumber() + ". " + item.getName() + " (" + item.getPosition().getCodePl() + ")");
                    }
                }
            });
        }

        @Override
        public void onEnteringPage(Wizard wizard) {
            Team hostTeam = (Team) getSettings().get("hostTeam");
            Team guestTeam = (Team) getSettings().get("guestTeam");
            Team sourceTeam = (Team) getSettings().get("sourceTeam");
            LocalDate matchDate = (LocalDate) getSettings().get("matchDate");

            if (sourceTeam == hostTeam) {
                hostTeam = hostTeam.deepClone();
                sourceTeam = hostTeam;
            } else {
                guestTeam = guestTeam.deepClone();
                sourceTeam = guestTeam;
            }


            selectionView.getSourceItems().setAll(sourceTeam.getPlayers());
            selectionView.getTargetItems().clear();

            invalidProperty().bind(leftToAssign.isNotEqualTo(0));

            getSettings().put("firstSquad", selectionView.getSourceItems());
            getSettings().put("subPlayers", selectionView.getTargetItems());


            MatchHistory matchHistory = new MatchHistory(hostTeam, guestTeam, sourceTeam, selectionView.getTargetItems(),matchDate);

            getSettings().put("matchHistory", matchHistory);
        }
    }
}
