package pl.mmaciega.matchanalyzer.ui.pitch;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.text.TextAlignment;
import pl.mmaciega.matchanalyzer.model.Player;
import pl.mmaciega.matchanalyzer.ui.ResizableMediaPlayer;
import uk.co.caprica.vlcj.player.direct.DirectMediaPlayer;

import java.io.IOException;
import java.util.Optional;

public class PlayerGraphic extends Label {
    @FXML
    private Tooltip tooltip;

    private static FootballEventContextMenu footballEventContextMenu;
    private BooleanProperty dragging = new SimpleBooleanProperty(false);

    private ObjectProperty<Player> player = new SimpleObjectProperty<>();
    private PositionGraphic positionGraphic;
    ImageView imageView = new ImageView();

    public PlayerGraphic(Player player, PositionGraphic positionGraphic) {
        this();
        this.positionGraphic = positionGraphic;
        setPlayer(player);
    }

    public PlayerGraphic() {
        imageView.setFitHeight(16);
        imageView.setFitWidth(16);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/pitch/Player.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        enableFullDrag();
        enableStartDragAndDrop();
    }

    @FXML
    private void initialize() {
        tooltip.setTextAlignment(TextAlignment.CENTER);
        tooltip.setGraphic(imageView);
        setTooltip(tooltip);

        player.addListener((observable, oldValue, newValue) -> {
            textProperty().bind(newValue.numberProperty().asString());
            tooltip.textProperty().bind(Bindings.concat(newValue.numberProperty(), ". ", newValue.nameProperty(),
                    " (", Bindings.createStringBinding(() -> newValue.getPosition().getCodePl(), newValue.positionProperty(), newValue.getPosition().codePlProperty())
                    , ")"));
            imageView.imageProperty().bind(Bindings.createObjectBinding(() -> newValue.getCountry().getImageFlag(), newValue.countryProperty()));
        });

    }

    private void enableFullDrag() {
        setOnMouseReleased(event -> {
            setMouseTransparent(false);
            dragging.set(false);
        });

        setOnMouseClicked(event -> {
            if (event.getButton() == MouseButton.PRIMARY) {
                footballEventContextMenu.setSelfEventType(getPlayer());
                footballEventContextMenu.show(this, event.getScreenX(), event.getScreenY());
            }
        });

        setOnDragDetected(event -> {
            setMouseTransparent(true);
            dragging.set(true);
            startFullDrag();
        });

        setOnMouseDragReleased((MouseDragEvent event) -> {
            if (event.getButton() == MouseButton.PRIMARY) {
                footballEventContextMenu.setTargetEventType(((PlayerGraphic) event.getGestureSource()).getPlayer(), getPlayer());
                footballEventContextMenu.show(this, event.getScreenX(), event.getScreenY());
            } else if (event.getButton() == MouseButton.SECONDARY) {
                PlayerGraphic sourcePlayer = (PlayerGraphic) event.getGestureSource();
                PlayerGraphic targetPlayer = (PlayerGraphic) event.getSource();

                int targetColumIndex = GridPane.getColumnIndex(targetPlayer),
                        targetRowIndex = GridPane.getRowIndex(targetPlayer),
                        targetColumnSpan = GridPane.getColumnSpan(targetPlayer),
                        targetRowSpan = GridPane.getRowSpan(targetPlayer);

                GridPane.setConstraints(targetPlayer, GridPane.getColumnIndex(sourcePlayer), GridPane.getRowIndex(sourcePlayer), GridPane.getColumnSpan(sourcePlayer), GridPane.getRowSpan(sourcePlayer));
                GridPane.setConstraints(sourcePlayer, targetColumIndex, targetRowIndex, targetColumnSpan, targetRowSpan);

                PositionGraphic targetPlayerPositionGraphic = targetPlayer.getPositionGraphic();
                targetPlayer.setPositionGraphic(sourcePlayer.getPositionGraphic());
                sourcePlayer.setPositionGraphic(targetPlayerPositionGraphic);
            }
        });


        setOnMouseDragEntered((MouseDragEvent event) -> {
            PlayerGraphic playerGraphic = (PlayerGraphic) event.getSource();
            tooltip.show(playerGraphic, event.getScreenX(), event.getScreenY());
        });

        setOnMouseDragExited(event -> tooltip.hide());
    }

    private void enableStartDragAndDrop() {

        setOnDragDropped(event1 -> {
            Dragboard dragboard = event1.getDragboard();
            boolean success = false;
            Optional<Player> playerOptional = getPlayer().getTeam().getPlayers().stream().filter(player1 -> player1.getId().equals(dragboard.getString())).findFirst();
            if (playerOptional.isPresent()) {
                TableCell<Player, Number> cell = (TableCell<Player, Number>) event1.getGestureSource();
                TableView<Player> tableView = cell.getTableView();
                int index = cell.getIndex();
                tableView.getItems().remove(cell.getIndex());
                tableView.getItems().add(index, this.getPlayer());
                tableView.getSelectionModel().select(index);
                setPlayer(playerOptional.get());
                success = true;
            }

            event1.setDropCompleted(success);
            event1.consume();
        });

        setOnDragOver(event -> {
            if (event.getGestureSource() instanceof TableCell) {
                event.acceptTransferModes(TransferMode.MOVE);
                event.consume();
            }
        });
    }

    public Player getPlayer() {
        return player.get();
    }

    public void setPlayer(Player player) {
        this.player.set(player);
    }

    public static void setFootballEventContextMenu(FootballEventContextMenu footballEventContextMenu) {
        PlayerGraphic.footballEventContextMenu = footballEventContextMenu;
    }

    public PositionGraphic getPositionGraphic() {
        return positionGraphic;
    }

    public void setPositionGraphic(PositionGraphic positionGraphic) {
        this.positionGraphic = positionGraphic;
    }

    public BooleanProperty draggingProperty() {
        return dragging;
    }
}
