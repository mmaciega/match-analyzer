package pl.mmaciega.matchanalyzer.ui.pitch;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.mmaciega.matchanalyzer.model.Position;
import pl.mmaciega.matchanalyzer.util.jaxb.wrapers.PositionsListWrapper;

import java.io.IOException;
import java.util.Optional;

public class PositionGraphic extends Label {
    private final Logger logger = LoggerFactory.getLogger(PositionGraphic.class);

    private Position position;

    public PositionGraphic(Position position) {
        loadXml();
        this.position = position;
    }

    public PositionGraphic() {
        loadXml();
        textProperty().addListener((observable, oldValue, newValue) -> {
            Optional<Position> positionOptional = PositionsListWrapper.findPositionByCodePl(newValue.replaceAll("\\d", ""));
            if (positionOptional.isPresent()) {
                position = positionOptional.get();
            } else {
                logger.warn("Błędny kod pozycji. Nie znaleziono odpowiadającego obiektu. Kod: " + newValue);
            }
        });
    }

    private void loadXml() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/pitch/Position.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        enableDragAndDrop();
    }

    private void enableDragAndDrop() {
        setOnMouseDragReleased(event -> {
            PlayerGraphic playerGraphic = (PlayerGraphic) event.getGestureSource();
            PositionGraphic positionGraphic = (PositionGraphic) event.getSource();

            System.out.println("Zmiana pozycji na " + positionGraphic.getText() + " " + positionGraphic.getPosition().getCodePl() + " " + System.identityHashCode(positionGraphic.getPosition()));
            GridPane.setColumnIndex(playerGraphic, GridPane.getColumnIndex((Node) event.getSource()));
            GridPane.setRowIndex(playerGraphic, GridPane.getRowIndex((Node) event.getSource()));

            playerGraphic.setPositionGraphic(positionGraphic);
        });

        setOnMouseDragEntered(event -> getStyleClass().add("on-drag-hover"));
        setOnMouseDragExited(event -> getStyleClass().remove("on-drag-hover"));
    }

    public Position getPosition() {
        return position;
    }
}
