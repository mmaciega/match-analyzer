package pl.mmaciega.matchanalyzer.ui.pitch;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import pl.mmaciega.matchanalyzer.model.Player;
import pl.mmaciega.matchanalyzer.model.events.DirectEvent;
import pl.mmaciega.matchanalyzer.model.events.FootballEvent;

import java.io.IOException;
import java.util.Set;

public class FootballEventContextMenu extends ContextMenu {
    private int numStartItems;

    private enum EventTypes {
        SELF {
            @Override
            public Set<FootballEvent.Types> getItems() {
                return FootballEvent.SELF_TYPES;
            }
        },
        TARGET {
            @Override
            public Set<FootballEvent.Types> getItems() {
                return FootballEvent.TARGET_TYPES;
            }
        };

        public abstract Set<FootballEvent.Types> getItems();
    }

    @FXML
    private ToggleGroup region;
    @FXML
    private CheckBox successful;
    @FXML
    private GridPane pitchPane;

    private EventTypes actualEventType;

    private Tooltip warningTooltip;
    private Player sourcePlayer, destinationPlayer;

    private FootballEvent result;

    public FootballEventContextMenu() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/pitch/FootballEventContextMenu.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

    }

    @FXML
    private void initialize() {
        numStartItems = getItems().size();
        warningTooltip = new Tooltip("Wybierz region boiska!");
        warningTooltip.setId("warning-tooltip");
        region.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            warningTooltip.hide();
        });
    }

    @Override
    public void show(Node anchor, double screenX, double screenY) {
        super.show(anchor, screenX, screenY);

        successful.setSelected(true);
        if (region.getSelectedToggle() != null) {
            region.getSelectedToggle().setSelected(false);
        }
        result = null;
    }

    public void setSelfEventType(Player sourcePlayer) {
        this.sourcePlayer = sourcePlayer;
        this.destinationPlayer = null;
        setEventType(EventTypes.SELF);
    }

    public void setTargetEventType(Player sourcePlayer, Player destinationPlayer) {
        this.sourcePlayer = sourcePlayer;
        this.destinationPlayer = destinationPlayer;
        setEventType(EventTypes.TARGET);
    }

    private void setEventType(EventTypes eventType) {
        actualEventType = eventType;

        getItems().remove(numStartItems, getItems().size());
        eventType.getItems().forEach((FootballEvent.Types type) -> {
            Label eventName = new Label(type.getName());
            eventName.setFont(new Font("System", 11));

            CustomMenuItem customMenuItem = new CustomMenuItem();
            customMenuItem.setContent(eventName);
            customMenuItem.setUserData(type);

            customMenuItem.hideOnClickProperty().bind(region.selectedToggleProperty().isNotNull());
            eventName.setOnMouseClicked(event1 -> {
                if (!customMenuItem.isHideOnClick()) {
                    Point2D point2D = pitchPane.localToScreen(pitchPane.getWidth() - 30, pitchPane.getHeight() / 2);
                    warningTooltip.hide();
                    warningTooltip.show(pitchPane, point2D.getX(), point2D.getY());
                }
            });

            customMenuItem.setOnAction(event -> {
                MenuItem menuItem1 = (MenuItem) event.getSource();
                if (customMenuItem.isHideOnClick()) {
                    createResult(menuItem1);
                }
            });

            getItems().add(customMenuItem);
        });

    }

    private FootballEvent.Regions getRegion() {
        return region.getSelectedToggle() != null ? FootballEvent.Regions.valueOf(((ToggleButton) region.getSelectedToggle()).getText()) : null;
    }

    private boolean isSuccessful() {
        return successful.isSelected();
    }

    private void createResult(MenuItem menuItem1) {
        switch (actualEventType) {
            case SELF:
                result = new FootballEvent(sourcePlayer, null, getRegion(),
                        (FootballEvent.Types) menuItem1.getUserData(), isSuccessful());
                break;
            case TARGET:
                result = new DirectEvent(sourcePlayer, destinationPlayer, null,
                        getRegion(), (FootballEvent.Types) menuItem1.getUserData(),
                        isSuccessful());
                break;
        }
    }

    public FootballEvent getResult() {
        return result;
    }
}
