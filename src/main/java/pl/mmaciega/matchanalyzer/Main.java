package pl.mmaciega.matchanalyzer;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.mmaciega.matchanalyzer.controller.MainController;
import pl.mmaciega.matchanalyzer.ui.ResizableMediaPlayer;
import pl.mmaciega.matchanalyzer.util.ResourceFileManager;
import pl.mmaciega.matchanalyzer.util.os.OSFolders;

import java.awt.*;
import java.io.IOException;

public class Main extends Application {
    static {
        System.setProperty("logback.directory", OSFolders.getUserData());
    }

    public static final String APP_TITLE = "Match analyzer";
    private static final Image APP_ICON = ResourceFileManager.getImage("icon.png");

    private final Logger logger = LoggerFactory.getLogger(Main.class);

    private boolean vlcLibsFound;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void init() {
        DataManager.init();

        try {
            ResizableMediaPlayer.loadNativeLibraries();
            vlcLibsFound = true;
        } catch (Throwable throwable) {
            vlcLibsFound = false;
        }
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        if (vlcLibsFound) {
            showApplication(primaryStage);
        } else {
            showNoVlcErrorDialog();
        }
    }

    private void showApplication(Stage primaryStage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(Main.class.getResource("/view/Main.fxml"));

        Region region = fxmlLoader.load();
        MainController controller = fxmlLoader.getController();
        controller.setStage(primaryStage);
        controller.setTeams(DataManager.getTeams());

        Scene scene = new Scene(region);
        primaryStage.setScene(scene);
        scene.getStylesheets().add(getClass().getResource("/css/Player.css").toExternalForm());
        scene.getStylesheets().add(getClass().getResource("/css/Position.css").toExternalForm());

        primaryStage.setOnCloseRequest(event -> {
            DataManager.saveTeamDatabase(controller.getTeams());
            ResizableMediaPlayer.getInstance().release();

            Platform.exit();
            System.exit(0);
        });

        primaryStage.getIcons().add(APP_ICON);
        primaryStage.setTitle(APP_TITLE);
        primaryStage.show();

        logger.info("Po show");

        primaryStage.setMinWidth(primaryStage.getWidth());
        primaryStage.setMinHeight(primaryStage.getHeight());

        SplashScreen splashScreen = SplashScreen.getSplashScreen();
        if (splashScreen != null) {
            splashScreen.close();
        }
    }

    private void showNoVlcErrorDialog() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(APP_ICON);
        alert.setTitle("Błąd uruchomienia");
        alert.setHeaderText("Błąd uruchomienia aplikacji");
        alert.setContentText("Nie znaleziono programu VideoLAN. Przed ponownym" +
                " uruchomieniem aplikacji zainstaluj wymagany program (http://www.videolan.org/).");

        alert.showAndWait();

        Platform.exit();
        System.exit(-1);
    }
}
